using System.Collections.Generic;
using UnityEngine;

namespace Lean.Touch
{
	// This script allows you to track & pedestral this GameObject (e.g. Camera) based on finger drags
	public class LeanCameraMove : MonoBehaviour
	{
		[Tooltip("The camera the movement will be done relative to (None = MainCamera)")]
		public Camera Camera;

		[Tooltip("Ignore fingers with StartedOverGui?")]
		public bool IgnoreStartedOverGui = true;

		[Tooltip("Ignore fingers with IsOverGui?")]
		public bool IgnoreIsOverGui;

		[Tooltip("Ignore fingers if the finger count doesn't match? (0 = any)")]
		public int RequiredFingerCount;

		[Tooltip("The sensitivity of the movement, use -1 to invert")]
		public float Sensitivity = 1.0f;

		public LeanScreenDepth ScreenDepth;
        public Vector2 xLimit;
        public Vector2 yLimit;

        [Range(0, 1)]
        public float SmoothValue;
        //[HideInInspector]
        public bool canMove = false;

        Vector3 moving = Vector3.zero;

		public virtual void SnapToSelection()
		{
			var center = default(Vector3);
			var count  = 0;

			for (var i = 0; i < LeanSelectable.Instances.Count; i++)
			{
				var selectable = LeanSelectable.Instances[i];

				if (selectable.IsSelected == true)
				{
					center += selectable.transform.position;
					count  += 1;
				}
			}

			if (count > 0)
			{
				transform.position = center / count;
			}
		}

		protected virtual void LateUpdate()
		{
            if (canMove)
            {
                // Get the fingers we want to use
                List<LeanFinger> fingers = LeanTouch.GetFingers(IgnoreStartedOverGui, IgnoreIsOverGui, RequiredFingerCount);
                Vector3 _tmp = transform.position;
                if (fingers.Count == 1)
                {
                    // Get the last and current screen point of all fingers
                    var lastScreenPoint = LeanGesture.GetLastScreenCenter(fingers);
                    var screenPoint = LeanGesture.GetScreenCenter(fingers);

                    // Get the world delta of them after conversion
                    var worldDelta = ScreenDepth.ConvertDelta(lastScreenPoint, screenPoint, Camera, gameObject);
                    
                    

                    float xCheck = _tmp.x - worldDelta.x * Sensitivity;
                    float zCheck = _tmp.z - worldDelta.z * Sensitivity;
                    //Debug.Log("xCheck " + xCheck);
                    //Debug.Log("yCheck " + zCheck);
                    /*if (xCheck > moving.x + xLimit.x && xCheck < moving.x + xLimit.y)
                    {
                        _tmp.x -= worldDelta.x * Sensitivity;
                    }
                    
                    if (zCheck > moving.z + yLimit.x && zCheck < moving.z + yLimit.y)
                    {
                        _tmp.y -= worldDelta.y * Sensitivity;
                        _tmp.z -= worldDelta.z * Sensitivity;
                    }*/

                    _tmp -= worldDelta * Sensitivity;

                    transform.position = _tmp;
                    //moving = _tmp;
                    //moving -= worldDelta * Sensitivity;
                    //Debug.Log("moving: " + moving);
                    //Camera.gameObject.transform.position = SmoothMove(Camera.gameObject.transform.position, _tmp, SmoothValue);

                }
            }
		}

        Vector3 SmoothMove(Vector3 _lastpos, Vector3 _pos, float _c)
        {
            return (_pos - _lastpos) * _c + _lastpos;
            //transform.position = Vector3.Lerp(transform.position, _pos, 30 * Time.deltaTime);
        }

        public void SetCanMove(bool _canMove)
        {
            canMove = _canMove;
            if (canMove)
            {
                //moving = Vector3.zero;
                moving = transform.position;
                //Debug.Log("moving: " + moving);
            }
        }
	}
}