using System.Collections.Generic;
using UnityEngine;

namespace Lean.Touch
{
	// This script allows you to zoom a camera in and out based on the pinch gesture
	// This supports both perspective and orthographic cameras
	[ExecuteInEditMode]
	public class LeanCameraZoom : MonoBehaviour
	{
		[Tooltip("The camera that will be zoomed (None = MainCamera)")]
		public Camera Camera;

		[Tooltip("Ignore fingers with StartedOverGui?")]
		public bool IgnoreStartedOverGui = true;

		[Tooltip("Ignore fingers with IsOverGui?")]
		public bool IgnoreIsOverGui;

		[Tooltip("Allows you to force rotation with a specific amount of fingers (0 = any)")]
		public int RequiredFingerCount;

		[Tooltip("If RequiredSelectable.IsSelected is false, ignore?")]
		public LeanSelectable RequiredSelectable;

		[Tooltip("If you want the mouse wheel to simulate pinching then set the strength of it here")]
		[Range(-1.0f, 1.0f)]
		public float WheelSensitivity;

		[Tooltip("Should the scaling be performanced relative to the finger center?")]
		public bool Relative;

		[Tooltip("The current FOV/Size")]
		public float Zoom = 50.0f;

		[Tooltip("Limit the FOV/Size?")]
		public bool ZoomClamp;

		[Tooltip("The minimum FOV/Size we want to zoom to")]
		public float ZoomMin = 10.0f;

		[Tooltip("The maximum FOV/Size we want to zoom to")]
		public float ZoomMax = 60.0f;

        public bool canUse = false;
        

        Vector3 startLook;
        Vector3 newLook;
        Vector3 targetLook;

        Vector2 xMovingLimit;
        Vector2 zMovingLimit;
        //bool changeRotate = false;

#if UNITY_EDITOR
        protected virtual void Reset()
		{
			Start();
		}
#endif

		protected virtual void Start()
		{
			if (RequiredSelectable == null)
			{
				RequiredSelectable = GetComponent<LeanSelectable>();
			}
            startLook = transform.eulerAngles;
            targetLook = startLook;
            newLook = new Vector3(startLook.x - 20, startLook.y, startLook.z);
            UpdateMovingLimit();
        }

		protected virtual void LateUpdate()
		{
            if (canUse)
            {
                // Get the fingers we want to use
                var fingers = LeanSelectable.GetFingers(IgnoreStartedOverGui, IgnoreIsOverGui, RequiredFingerCount, RequiredSelectable);

                if (fingers.Count == 2) {
                    // Get the pinch ratio of these fingers
                    var pinchRatio = LeanGesture.GetPinchRatio(fingers, WheelSensitivity);

                    // Perform the translation if this is a relative scale
                    if (Relative == true)
                    {
                        var pinchScreenCenter = LeanGesture.GetScreenCenter(fingers);

                        Translate(pinchRatio, pinchScreenCenter);
                    }

                    SetZoom(pinchRatio, fingers);
                }
            }
		}

		protected virtual void Translate(float pinchScale, Vector2 screenCenter)
		{
			// Make sure the camera exists
			var camera = LeanTouch.GetCamera(Camera, gameObject);

			if (camera != null)
			{
				// Screen position of the transform
				var screenPosition = camera.WorldToScreenPoint(transform.position);

				// Push the screen position away from the reference point based on the scale
				screenPosition.x = screenCenter.x + (screenPosition.x - screenCenter.x) * pinchScale;
				screenPosition.y = screenCenter.y + (screenPosition.y - screenCenter.y) * pinchScale;

				// Convert back to world space
				transform.position = camera.ScreenToWorldPoint(screenPosition);
			}
			else
			{
				Debug.LogError("Failed to find camera. Either tag your cameras MainCamera, or set one in this component.", this);
			}
		}

		protected void SetZoom(float current, List<LeanFinger> leanFingers)
		{
			// Make sure the camera exists
			var camera = LeanTouch.GetCamera(Camera, gameObject);

			if (camera != null)
			{
				if (camera.orthographic == true)
				{
					camera.orthographicSize = current;
				}
				else
				{
                    current = 1 - current;
                    if ((Zoom + current) >= ZoomMin && (Zoom + current) < ZoomMax) {
                        Zoom += current;
                        Zoom = Mathf.Clamp(Zoom, ZoomMin, ZoomMax);
                        Vector3 FingersCenterPos = (leanFingers[0].GetWorldPosition(0.5f, camera) + leanFingers[1].GetWorldPosition(0.5f, camera)) / 2;
                        Vector3 Direction = FingersCenterPos - transform.position;
                        
                        transform.position += Direction * current;
                        if (Camera.gameObject.GetComponent<SecondCamera>())
                        {
                            //Camera.gameObject.GetComponent<SecondCamera>().GetCurrentCountry().SetPinsScale(Zoom);
                        }
                        if (Camera.gameObject.GetComponent<ThirdCamera>())
                        {
                            Camera.gameObject.GetComponent<ThirdCamera>().GetCurrentArea().SetPointsScale(Zoom);
                        }

                        gameObject.GetComponent<LimitCameraMoving>().xLimit = xMovingLimit * (1 + Zoom);
                        gameObject.GetComponent<LimitCameraMoving>().zLimit = zMovingLimit * (1 + Zoom);
                    }
                }
			}
			else
			{
				Debug.LogError("Failed to find camera. Either tag your cameras MainCamera, or set one in this component.", this);
			}
		}

        public void SetState(bool _canUse)
        {
            canUse = _canUse;
            if (canUse)
            {
                Zoom = 0;
            }
        }

        public void UpdateMovingLimit() {
            xMovingLimit = gameObject.GetComponent<LimitCameraMoving>().xLimit;
            zMovingLimit = gameObject.GetComponent<LimitCameraMoving>().zLimit;
        }
	}
}