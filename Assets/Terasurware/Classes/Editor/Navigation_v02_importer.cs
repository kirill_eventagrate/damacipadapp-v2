using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;
using System.Xml.Serialization;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;

public class Navigation_v02_importer : AssetPostprocessor {
	private static readonly string filePath = "Assets/ExelData/Navigation_v02.xlsx";
	private static readonly string exportPath = "Assets/ExelData/Navigation_v02.asset";
	private static readonly string[] sheetNames = { "English","Chinese", };
	
	static void OnPostprocessAllAssets (string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
	{
		foreach (string asset in importedAssets) {
			if (!filePath.Equals (asset))
				continue;
				
			Entity_English data = (Entity_English)AssetDatabase.LoadAssetAtPath (exportPath, typeof(Entity_English));
			if (data == null) {
				data = ScriptableObject.CreateInstance<Entity_English> ();
				AssetDatabase.CreateAsset ((ScriptableObject)data, exportPath);
				data.hideFlags = HideFlags.NotEditable;
			}
			
			data.sheets.Clear ();
			using (FileStream stream = File.Open (filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)) {
				IWorkbook book = null;
				if (Path.GetExtension (filePath) == ".xls") {
					book = new HSSFWorkbook(stream);
				} else {
					book = new XSSFWorkbook(stream);
				}
				
				foreach(string sheetName in sheetNames) {
					ISheet sheet = book.GetSheet(sheetName);
					if( sheet == null ) {
						Debug.LogError("[QuestData] sheet not found:" + sheetName);
						continue;
					}

					Entity_English.Sheet s = new Entity_English.Sheet ();
					s.name = sheetName;
				
					for (int i=1; i<= sheet.LastRowNum; i++) {
						IRow row = sheet.GetRow (i);
						ICell cell = null;
						
						Entity_English.ProjectParam p = new Entity_English.ProjectParam();
						
					cell = row.GetCell(0); p.ProjectId = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(1); p.Country = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(2); p.City = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(3); p.Area = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(4); p.Project = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(5); p.Discription = (cell == null ? "" : cell.StringCellValue);
						s.Projectslist.Add (p);
					}
					data.sheets.Add(s);
				}
			}

			ScriptableObject obj = AssetDatabase.LoadAssetAtPath (exportPath, typeof(ScriptableObject)) as ScriptableObject;
			EditorUtility.SetDirty (obj);
		}
	}
}
