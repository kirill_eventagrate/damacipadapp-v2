using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Entity_English : ScriptableObject
{	
	public List<Sheet> sheets = new List<Sheet> ();
    public List<BrochureListParam> Brochurelist = new List<BrochureListParam>();

    [System.SerializableAttribute]
	public class Sheet
	{
		public string name = string.Empty;
		public List<ProjectParam> Projectslist = new List<ProjectParam>();
        public List<AreaParam> Areaslist = new List<AreaParam>();
        public List<CityParam> Cityslist = new List<CityParam>();
        public List<CountryParam> Coutrieslist = new List<CountryParam>();
    }

    [System.SerializableAttribute]
	public class ProjectParam
	{
		public string ProjectId;
		public string Country;
		public string City;
		public string Area;
		public string Project;
		public string Discription;
	}

    [System.SerializableAttribute]
    public class AreaParam
    {
        public string AreaId;
        public string Area;
        public string ProjectIdFilters;
    }

    [System.SerializableAttribute]
    public class CityParam
    {
        public string CityId;
        public string City;
        public string AreaIdFilters;
    }

    [System.SerializableAttribute]
    public class CountryParam
    {
        public string CountryId;
        public string Country;
        public string CityIdFilters;
    }

    [System.SerializableAttribute]
    public class BrochureListParam
    {
        public string ProjectId;
        public string Project;
        public string URLofBrochure;
    }
}

public enum ExcelParam {
    ProjectId,
    Country,
    City,
    Area,
    Project,
    Discription
}

