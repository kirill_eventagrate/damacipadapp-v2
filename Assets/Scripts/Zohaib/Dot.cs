﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dot : MonoBehaviour {

    public iTweenAnimation dotAnimation, dotAnimation_1, dotAnimation_2;

    RectTransform dotRt, dotRt_1, dotRt_2;
    Image dotLine, dotLine_1, dotLine_2;

    IEnumerator Start()
    {
        dotLine = dotAnimation.transform.GetComponent<Image>();
        dotLine_1 = dotAnimation_1.transform.GetComponent<Image>();
        dotLine_2 = dotAnimation_2.transform.GetComponent<Image>();

        dotRt = dotAnimation.transform.GetComponent<RectTransform>();
        dotRt_1 = dotAnimation_1.transform.GetComponent<RectTransform>();
        dotRt_2 = dotAnimation_2.transform.GetComponent<RectTransform>();

        dotAnimation.iTweenStart();
        yield return new WaitForSeconds(1);
        dotAnimation_1.iTweenStart();
        yield return new WaitForSeconds(1);
        dotAnimation_2.iTweenStart();
    }

    void UpdateDotScale(float val)
    {
        dotRt.localScale = new Vector3(val, val, val);
    }

    void UpdateDotScale_1(float val)
    {
        dotRt_1.localScale = new Vector3(val, val, val);
    }

    void UpdateDotScale_2(float val)
    {
        dotRt_2.localScale = new Vector3(val, val, val);
    }

    void OnCompleteDot()
    {
        dotAnimation.iTweenStart();
    }

    void OnCompleteDot_1()
    {
        dotAnimation_1.iTweenStart();
    }

    void OnCompleteDot_2()
    {
        dotAnimation_2.iTweenStart();
    }

    void UpdateDotFade(float val)
    {
        dotLine.color = new Color(dotLine.color.r, dotLine.color.g, dotLine.color.b, val);
    }

    void UpdateDotFade_1(float val)
    {
        dotLine_1.color = new Color(dotLine_1.color.r, dotLine_1.color.g, dotLine_1.color.b, val);
    }

    void UpdateDotFade_2(float val)
    {
        dotLine_2.color = new Color(dotLine_2.color.r, dotLine_2.color.g, dotLine_2.color.b, val);
    }

}
