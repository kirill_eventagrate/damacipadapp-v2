﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using System.IO;

public class Screensaver : MonoBehaviour
{
    float screensaverTime = 10.0f;
    float time = 0;

    public VideoPlayer videoPlayer;

    public GameObject[] componentsToShow;

    private void Start()
    {
        string screensaverPath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Screensaver";

        if (!Directory.Exists(screensaverPath))
        {
            Directory.CreateDirectory(screensaverPath);
        }
        string[] allScreensaverFiles = Directory.GetFiles(screensaverPath, "*.mp4");

        if (allScreensaverFiles.Length != 0)
        {
            videoPlayer.url = allScreensaverFiles[0];
        }
        
        ShowComponents(false);
    }

    void ShowComponents(bool val)
    {
        foreach (GameObject item in componentsToShow)
        {
            item.SetActive(val);
        }
    }

    private void Update()
    {
        time += Time.deltaTime;

        if (time >= screensaverTime)
        {
            if (videoPlayer.url != "" && !videoPlayer.isPlaying)
            {
                ShowComponents(true);
                videoPlayer.Play();
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            videoPlayer.Stop();
            ShowComponents(false);
            time = 0;
        }
    }
}
