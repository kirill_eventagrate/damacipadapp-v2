﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml;
using System.Linq;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;

public class ExcelImportRunTime 
{

    private string filePath = "Assets/ExelData/Navigation_v02.xlsx";
    static string root = "";

    

    public Entity_English ReadExcel(Entity_English _exelData, string _fileName)
    {
        //sheetNames.Clear();
        _exelData.sheets.Clear();

        root = Application.persistentDataPath + "/";
        filePath = root + _fileName;
        using (FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
        {
            IWorkbook book = null;
            if (Path.GetExtension(filePath) == ".xls")
            {
                book = new HSSFWorkbook(stream);
            }
            else
            {
                book = new XSSFWorkbook(stream);
            }
            foreach (var Language in FileManager.instance.Settings["Languages"]) {
                string sheetName = Language.ToString();
                ISheet sheet = book.GetSheet(sheetName);
                if (sheet == null)
                {
                    Debug.LogError("[QuestData] sheet not found:" + sheetName);
                    continue;
                }

                Entity_English.Sheet s = new Entity_English.Sheet();
                s.name = sheetName;

                for (int i = 1; i <= sheet.LastRowNum; i++)
                {
                    IRow row = sheet.GetRow(i);
                    ICell cell = null;

                    Entity_English.ProjectParam p = new Entity_English.ProjectParam();

                    cell = row.GetCell(0); p.ProjectId = (cell == null ? "" : cell.NumericCellValue.ToString());
                    cell = row.GetCell(1); p.Country = (cell == null ? "" : cell.StringCellValue);
                    cell = row.GetCell(2); p.City = (cell == null ? "" : cell.StringCellValue);
                    cell = row.GetCell(3); p.Area = (cell == null ? "" : cell.StringCellValue);
                    cell = row.GetCell(4); p.Project = (cell == null ? "" : cell.StringCellValue);
                    cell = row.GetCell(5); p.Discription = (cell == null ? "" : cell.StringCellValue);
                    s.Projectslist.Add(p);

                    if (i < 6)
                    {
                        Entity_English.CountryParam CountryP = new Entity_English.CountryParam();
                        cell = row.GetCell(15); CountryP.CountryId = (cell == null ? "" : cell.StringCellValue);
                        cell = row.GetCell(16); CountryP.Country = (cell == null ? "" : cell.StringCellValue);
                        cell = row.GetCell(17); CountryP.CityIdFilters = (cell == null ? "" : cell.StringCellValue);
                        s.Coutrieslist.Add(CountryP);
                    }

                    if (i < 8)
                    {
                        Entity_English.CityParam CityP = new Entity_English.CityParam();
                        cell = row.GetCell(11); CityP.CityId = (cell == null ? "" : cell.StringCellValue);
                        cell = row.GetCell(12); CityP.City = (cell == null ? "" : cell.StringCellValue);
                        cell = row.GetCell(13); CityP.AreaIdFilters = (cell == null ? "" : cell.StringCellValue);
                        s.Cityslist.Add(CityP);
                    }

                    if (i < 17) {
                        Entity_English.AreaParam AreaP = new Entity_English.AreaParam();
                        cell = row.GetCell(7); AreaP.AreaId = (cell == null ? "" : cell.StringCellValue);
                        cell = row.GetCell(8); AreaP.Area = (cell == null ? "" : cell.StringCellValue);
                        cell = row.GetCell(9); AreaP.ProjectIdFilters = (cell == null ? "" : cell.StringCellValue);
                        s.Areaslist.Add(AreaP);
                    }
                }
                _exelData.sheets.Add(s);
            }
            string brochureSheetName = FileManager.instance.Settings["BrochureListName"].ToString();
            ISheet brochureSheet = book.GetSheet(brochureSheetName);
            if (brochureSheet != null)
            {
                for (int i = 1; i <= brochureSheet.LastRowNum; i++) {
                    IRow row = brochureSheet.GetRow(i);
                    ICell cell = null;

                    Entity_English.BrochureListParam p = new Entity_English.BrochureListParam();

                    cell = row.GetCell(0); p.ProjectId = (cell == null ? "" : cell.NumericCellValue.ToString());
                    cell = row.GetCell(1); p.Project = (cell == null ? "" : cell.StringCellValue);
                    cell = row.GetCell(2); p.URLofBrochure = (cell == null ? "" : cell.StringCellValue);
                    _exelData.Brochurelist.Add(p);
                }
            }
        }
        return _exelData;
    }
}
