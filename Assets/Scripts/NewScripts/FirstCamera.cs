﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstCamera : MonoBehaviour
{
    static public FirstCamera instance = null;
    public GameObject Parent;

    Vector3 startPos;
    Vector3 statLook;

    [HideInInspector]
    public bool chooseCountry = false;

    void Awake()
    {
        if (instance == null)
            instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
        statLook = transform.rotation.eulerAngles;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MovingToCountry(Transform _transform, Vector2 _orbitcameraTarget)
    {
        if (!chooseCountry)
        {
            //Debug.Log("MovingToCountry");
            chooseCountry = true;
            Parent.GetComponent<CameraOrbit>().SetTarget(_orbitcameraTarget);
            Loom.QueueOnMainThread(() => {
                statLook = transform.rotation.eulerAngles;

                Parent.GetComponent<CameraOrbit>().down = false;
                Parent.GetComponent<CameraOrbit>().enabled = false;
                startPos = transform.position;
                iTween.MoveTo(gameObject, iTween.Hash("position", _transform, "time", 1.5f, "delay", 0f, "easetype", iTween.EaseType.easeOutSine));
                //iTween.LookTo(gameObject, iTween.Hash("looktarget", _transform, "time", 1.5f, "delay", 0f, "easetype", iTween.EaseType.easeOutSine));
            }, 1f);

        }
    }

    public void MoveBack(float _delay)
    {
        iTween.MoveTo(gameObject, iTween.Hash("position", startPos, "time", 1.5f, "delay", _delay, "easetype", iTween.EaseType.easeOutSine));
        iTween.RotateTo(gameObject, iTween.Hash("rotation", statLook, "time", 1.5f, "delay", _delay, "easetype", iTween.EaseType.easeOutSine));
        Invoke("TurnOnCameraOrbit", 1.5f);
    }

    void TurnOnCameraOrbit()
    {
        Parent.GetComponent<CameraOrbit>().enabled = true;
        chooseCountry = false;
    }
}
