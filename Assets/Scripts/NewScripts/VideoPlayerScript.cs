﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using System;

public class VideoPlayerScript : MonoBehaviour {



	public Action OnVideoReadyToPlay = () => {};
	public Action OnVideoCompleted = () => {};

	public RawImage rawImage;
	public VideoPlayer player;
	public AudioSource audioSource;
    public Button playBtn;

	bool isPlaying;



	// Use this for initialization
	void Start () {
        InitPlayer();
        playBtn.onClick.AddListener(() => PlayPauseVideo());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// METHODS
	bool isIntialized = false;
	void InitPlayer(){
		if (isIntialized)
			return;

        
		player.playOnAwake = false;
		//player.renderMode = VideoRenderMode.APIOnly;
		//player.source = VideoSource.Url;
		player.audioOutputMode = VideoAudioOutputMode.AudioSource;

		player.controlledAudioTrackCount = 1;
		player.EnableAudioTrack(0, true);
		player.SetTargetAudioSource(0, audioSource);


		player.prepareCompleted += (VideoPlayer source) => {
			rawImage.texture = player.texture;
			rawImage.GetComponent<AspectRatioFitter>().aspectRatio = (float)rawImage.texture.width / rawImage.texture.height;

			OnVideoReadyToPlay();
            //Play();
        };

		player.loopPointReached += (source) => {
			OnVideoCompleted();
		};

		isIntialized = true;
	}

	public void LoadVideo(string fullPath){
		InitPlayer ();

		player.url =  fullPath;
		player.Prepare ();
	}

	public void Play(){
		InitPlayer ();

		player.Play ();
		audioSource.Play();
	}

	public void Pause(){
		InitPlayer ();

		if (player.isPlaying) {
			player.Pause ();
            audioSource.Pause();
        }
	}

    public void Stop()
    {
        InitPlayer();

        player.Stop();
        audioSource.Stop();
    }

    void PlayPauseVideo()
    {
        if (player.isPlaying)
        {
            player.Pause();
            playBtn.gameObject.GetComponent<Image>().enabled = true;
        }
        else
        {
            player.Play();
            playBtn.gameObject.GetComponent<Image>().enabled = false;
        }
    }

    // EVENTS
}
