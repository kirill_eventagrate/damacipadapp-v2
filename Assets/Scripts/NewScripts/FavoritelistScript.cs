﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FavoritelistScript : MonoBehaviour
{
    static public FavoritelistScript instance = null;

    public GameObject FavoritesContainer;
    public GameObject FavoritesPrefab;
    public Button ShareBtn;
    public Button ClearBtn;
    public GameObject SendProjectPanel;
    public Text CounterText;

    public List<ContentItem> Favoritelist = new List<ContentItem>();

    Animator animator;
    bool favoritesIsShow = false;

    void Awake()
    {
        if (instance == null)
            instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        UpdateList();
        ShareBtn.onClick.AddListener(() => OpenShareProjectsPanel());
        ClearBtn.onClick.AddListener(() => ClearFavourites());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddFavorite(ContentItem _content)
    {
        if (!HasFavorites(_content.ExelIndexName))
        {
            Favoritelist.Add(_content);
            GameObject obj = Instantiate(FavoritesPrefab, FavoritesContainer.transform) as GameObject;
            obj.GetComponent<FavoriteRow>().SetData(_content);
        }
        else
        {
            RemoveFavorite(_content);
        }
        CounterText.text = Favoritelist.Count.ToString();
        CounterText.transform.parent.gameObject.GetComponent<Animator>().Play("FavouritCounterAnim", -1, 0);
    }

    public bool HasFavorites(int _ExelIndexName)
    {
        foreach (ContentItem favorite in Favoritelist)
        {
            if (favorite.ExelIndexName == _ExelIndexName)
            {
                return true;
            }
        }
        return false;
    }

    public void RemoveFavorite(ContentItem _content)
    {
        Favoritelist.Remove(_content);
        for (int i = 0; i < FavoritesContainer.transform.childCount; i++)
        {
            GameObject child = FavoritesContainer.transform.GetChild(i).gameObject;
            if (child.GetComponent<FavoriteRow>().content == _content) {
                child.GetComponent<FavoriteRow>().DestroyObj();
            }
        }
        CounterText.text = Favoritelist.Count.ToString();
        CounterText.transform.parent.gameObject.GetComponent<Animator>().Play("FavouritCounterAnim", -1, 0);
    }

    void ClearFavourites() {
        for (int i = 0; i < FavoritesContainer.transform.childCount; i++)
        {
            GameObject child = FavoritesContainer.transform.GetChild(i).gameObject;
            ContentItem _content = child.GetComponent<FavoriteRow>().content;
            Favoritelist.Remove(_content);
            child.GetComponent<FavoriteRow>().DestroyObj();
        }
        CounterText.text = Favoritelist.Count.ToString();
        CounterText.transform.parent.gameObject.GetComponent<Animator>().Play("FavouritCounterAnim", -1, 0);
    }

    public void UpdateList()
    {
        for (int i = 0; i < FavoritesContainer.transform.childCount; i++)
        {
            Destroy(FavoritesContainer.transform.GetChild(i).gameObject);
        }

        for(int i = 0; i < Favoritelist.Count; i++)
        {
            GameObject obj = Instantiate(FavoritesPrefab, FavoritesContainer.transform) as GameObject;
            obj.GetComponent<FavoriteRow>().SetData(Favoritelist[i]);
        }
        CounterText.text = Favoritelist.Count.ToString();
    }

    public void ShowHideFavoritesList()
    {
        if (!favoritesIsShow)
        {
            animator.SetFloat("Direction", 1f);
            animator.Play("Favories", -1, 0f);
            favoritesIsShow = true;
        }
        else
        {
            animator.SetFloat("Direction", -1f);
            animator.Play("Favories", -1, 1f);
            favoritesIsShow = false;
        }
        
    }

    public void OpenShareProjectsPanel()
    {
        SendProjectPanel.SetActive(true);
    }
}
