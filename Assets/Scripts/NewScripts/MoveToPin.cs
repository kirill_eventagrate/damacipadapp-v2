﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveToPin : MonoBehaviour
{
    public Button btn;
    public ExcelParam excelParam;
    public int ExelIndex = 2;

    public float yPosMove;
    public float zPosMove;

    // Start is called before the first frame update
    void Start()
    {
        btn.onClick.AddListener(() => Moving());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Moving() {
        Vector3 tmpPos = new Vector3(transform.position.x, transform.position.y + yPosMove, transform.position.z - zPosMove);
        SecondCamera.instance.SaveLastPos();
        SecondCamera.instance.MoveTo(tmpPos, 1.2f, 0);
        UIManager.instance.PutLegendBtn(ExelIndex, excelParam, () => {});
    }
}
