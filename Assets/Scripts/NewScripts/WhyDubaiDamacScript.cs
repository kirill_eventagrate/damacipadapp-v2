﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WhyDubaiDamacScript : MonoBehaviour
{
    static public WhyDubaiDamacScript instance = null;


    public string[] WhyDubaiLanguageTitle;
    public Button WhyDubai;
    public string[] WhyDAMACLanguageTitle;
    public Button WhyDAMAC;

    public Button Left;
    public Button Right;
    public Button Close;

    public SliderScript[] Sliders;


    public Image MainImage;
    public Image BackImage;

    public Animator animator;
    public Animator sliderAnimator;
    Animator LeftArrowAnim;
    Animator RightArrowAnim;


    bool SliderIsShowing = false;
    bool isShowContent = false;
    int LanguageIndex = 0;
    int SliderIndex = 0;

    void Awake()
    {
        if (instance == null)
            instance = this;

    }
    // Start is called before the first frame update
    void Start()
    {
        SetText();
        LeftArrowAnim = Left.gameObject.GetComponent<Animator>();
        RightArrowAnim = Right.gameObject.GetComponent<Animator>();
        WhyDubai.onClick.AddListener(() => OnBtnClick(WhyDubai));
        WhyDAMAC.onClick.AddListener(() => OnBtnClick(WhyDAMAC));

        Left.onClick.AddListener(() => {
            PrevSlide();
            LeftArrowAnim.Play("RightArrowAnim", -1, 0);
            RightArrowAnim.Play("RightArrowAnim", -1, 0);
        });
        Right.onClick.AddListener(() => {
            NextSldie();
            LeftArrowAnim.Play("RightArrowAnim", -1, 0);
            RightArrowAnim.Play("RightArrowAnim", -1, 0);
        });
        Close.onClick.AddListener(() => HideSlider(true));
    }

    // Update is called once per frame
    void Update()
    {
        if (LanguageIndex != FileManager.instance.LanguageIndex)
        {
            LanguageIndex = FileManager.instance.LanguageIndex;
            SetText();
        }
    }

    void SetText()
    {
        if (LanguageIndex < WhyDubaiLanguageTitle.Length)
        {
            WhyDubai.GetComponentInChildren<Text>().text = WhyDubaiLanguageTitle[LanguageIndex];
            WhyDAMAC.GetComponentInChildren<Text>().text = WhyDAMACLanguageTitle[LanguageIndex];
            Sliders[SliderIndex].UpdateLanguage(LanguageIndex);
        }
    }

    void NextSldie() {
        Sliders[SliderIndex].NextSlide(LanguageIndex);
        if (Sliders[SliderIndex].GetCorrectSlideIndex() == (Sliders[SliderIndex]._slide.Length - 1) ) {
            Right.gameObject.SetActive(false);
        }
        if (Sliders[SliderIndex].GetCorrectSlideIndex() > 0) {
            Left.gameObject.SetActive(true);
        }
    }

    void PrevSlide() {
        Sliders[SliderIndex].PrevSlide(LanguageIndex);

        if (Sliders[SliderIndex].GetCorrectSlideIndex() == 0)
        {
            Left.gameObject.SetActive(false);
        }
        if (Sliders[SliderIndex].GetCorrectSlideIndex() < (Sliders[SliderIndex]._slide.Length - 1))
        {
            Right.gameObject.SetActive(true);
        }

    }

    void OnBtnClick(Button _btn)
    {
        Left.gameObject.SetActive(false);
        Right.gameObject.SetActive(true);
        if (_btn == WhyDubai)
        {
            SetSelect(true, WhyDubai);
            SetSelect(false, WhyDAMAC);

            SliderIndex = 0;
            Sliders[SliderIndex].InitStartContent();
            if (!SliderIsShowing)
            {
                ShowSlider();
                Sliders[0].ShowSlider();
            }
            else
            {
                Sliders[0].ShowSlider();
                Sliders[1].HideSlider();
            }
        }
        else if (_btn == WhyDAMAC)
        {
            SetSelect(true, WhyDAMAC);
            SetSelect(false, WhyDubai);
            SliderIndex = 1;
            Sliders[SliderIndex].InitStartContent();
            if (!SliderIsShowing)
            {
                ShowSlider();
                Sliders[1].ShowSlider();
            }
            else
            {
                Sliders[0].HideSlider();
                Sliders[1].ShowSlider();
            }
        }

    }

    void SetSelect(bool isSelect, Button _btn)
    {
        Color selectColor = new Color(0.08235294f, 0.2470588f, 0.3254902f);
        if (isSelect)
        {
            _btn.GetComponent<Image>().color = Color.white;
            _btn.GetComponentInChildren<Text>().color = selectColor;
        }
        else
        {
            _btn.GetComponent<Image>().color = selectColor;
            _btn.GetComponentInChildren<Text>().color = Color.white;
        }
    }



    public void ShowContent()
    {
        isShowContent = true;
        animator.SetFloat("Direction", 1f);
        animator.Play("ShowContentWhyDubai", -1, 0f);
    }

    public void CloseContent()
    {
        /*SetSelect(false, WhyDubai);
        SetSelect(false, WhyDAMAC);*/
        if (isShowContent)
        {
            isShowContent = false;
            animator.SetFloat("Direction", -1f);
            animator.Play("ShowContentWhyDubai", -1, 1f);
            if (SliderIsShowing) {
                HideSlider(false);
            }
        }
    }



    void ShowSlider()
    {
        sliderAnimator.SetFloat("Direction", 1f);
        sliderAnimator.Play("ShowHideDubaiDamacSlider", -1, 0f);
        SliderIsShowing = true;
        SecondCamera.instance.ResetMovingState(false, false);
    }

    void HideSlider(bool _needResetState)
    {
        sliderAnimator.SetFloat("Direction", -1f);
        sliderAnimator.Play("ShowHideDubaiDamacSlider", -1, 1f);
        SliderIsShowing = false;

        SetSelect(false, WhyDubai);
        SetSelect(false, WhyDAMAC);

        Sliders[SliderIndex].HideSlider();

        if (_needResetState)
        {
            SecondCamera.instance.ResetMovingState(true, false);
        }
    }
}

public enum DubaiOrDamac {
    Dubai,
    Damac
}
