﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingPanelScript : MonoBehaviour
{
    static public SettingPanelScript instance = null;


    public List<Button> LanguageBtns = new List<Button>();
    public Button SettingBtn;
    public GameObject BtnsContainer;
    public GameObject BtnPrefab;

    bool PanelIsShowing = true;
    Animator animator;


    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        if (gameObject.GetComponent<Animator>())
        {
            animator = gameObject.GetComponent<Animator>();
        }
        SettingBtn.onClick.AddListener(() => ShowHidePanel());

        Loom.QueueOnMainThread(() => {
            InitBtns();
        }, 0.1f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    void InitBtns()
    {
        for (int i = 0; i < BtnsContainer.transform.childCount; i++)
        {
            Destroy(BtnsContainer.transform.GetChild(i).gameObject);
        }
        int j = 0;
        foreach (Entity_English.Sheet lang in FileManager.instance.exelData.sheets)
        {
            GameObject btn = Instantiate(BtnPrefab) as GameObject;
            btn.transform.SetParent(BtnsContainer.transform);
            btn.GetComponent<LanguageBtn>().SetData(lang.name, j);
            LanguageBtns.Add(btn.GetComponent<Button>());
            j++;
        }
        SelectBtn(LanguageBtns[0]);
        //gameObject.GetComponent<RectTransform>().localScale = new Vector3(1, 0, 1);
        ShowHidePanel();
        Loom.QueueOnMainThread(() => {
            gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(-235, -128);
        }, 1f);
    }

    public void SelectBtn(Button _btn)
    {
        foreach (Button langBtn in LanguageBtns)
        {
            if (langBtn == _btn)
            {
                Color SelectColor = new Color(0.4541652f, 0.4817996f, 0.509434f, 1);
                langBtn.GetComponent<Image>().color = SelectColor;
            }
            else
            {
                Color NormalColor = new Color(0.1529412f, 0.1843137f, 0.2169811f, 1); // 272F37
                langBtn.GetComponent<Image>().color = NormalColor;
            }
        }
    }



    public void ShowHidePanel()
    {
        if (!PanelIsShowing)
        {
            animator.SetFloat("Direction", 1f);
            animator.Play("ShowHideSettingPanel", -1, 0);
            PanelIsShowing = true;
        }
        else
        {
            animator.SetFloat("Direction", -1f);
            animator.Play("ShowHideSettingPanel", -1, 1);
            PanelIsShowing = false;
        }
    }
}
