﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityScript : MonoBehaviour
{
    public int CityExcelId;
    public GameObject Parent;
    public GameObject StartTarget;
    public GameObject EndTarget;
    public GameObject[] Points;

    public Animator animator;
    public string ShowAnimationName;
    public List<int> projectsList = new List<int>();
    public bool ShowWhayDamacPanel = false;

    Entity_English.CityParam cityParam = null;
    bool ShowContent = true;


    // Start is called before the first frame update
    void Start()
    {
        HidePoints(0.2f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InitCityData(int _cityExcelId)
    {
        CityExcelId = _cityExcelId;
        cityParam = FileManager.instance.GetCityParam(FileManager.instance.LanguageIndex, CityExcelId);
        string[] areaIdFilters = cityParam.AreaIdFilters.Split(',');
        for (int i = 0; i < Points.Length; i++)
        {
            if (i < areaIdFilters.Length)
            {
                int areaId = System.Convert.ToInt32(areaIdFilters[i].Trim());
                if (Points[i].GetComponent<PointItem>()) {
                    Points[i].GetComponent<PointItem>().SetAreaData(areaId);
                }
                Entity_English.AreaParam areaParam = FileManager.instance.GetAreaParam(FileManager.instance.LanguageIndex, areaId);
                string[] projectsIdFilters = areaParam.ProjectIdFilters.Split(',');
                projectsList.Clear();
                for (int j = 0; j < projectsIdFilters.Length; j++)
                {
                    int projectId = System.Convert.ToInt32(projectsIdFilters[j].Trim());
                    projectsList.Add(projectId);
                }
                /*if (Points[i].GetComponent<ProjectItem>()) {
                    Points[i].GetComponent<ProjectItem>().InitProjectData(areaId);
                }*/
            }
        }
    }


    public void ShowPoints(float _delay)
    {
        if (!ShowContent)
        {
            foreach (GameObject point in Points)
            {
                if (point.GetComponent<PointItem>())
                {
                    point.GetComponent<PointItem>().ShowPoint(_delay);
                }
            }
            if (animator != null)
            {
                animator.SetFloat("Direction", 1);
                animator.Play(ShowAnimationName, -1, 0);
            }
            ShowContent = true;
        }
    }

    public void HidePoints(float _delay)
    {
        if (ShowContent)
        {
            foreach (GameObject point in Points)
            {
                if (point.GetComponent<PointItem>())
                {
                    point.GetComponent<PointItem>().HidePoint(_delay);
                }
            }
            if (animator != null)
            {
                animator.SetFloat("Direction", -1);
                animator.Play(ShowAnimationName, -1, 1);
            }
            ShowContent = false;
        }
    }

    public List<ContentItem> GetContents()
    {
        List<ContentItem> _tmpContent = new List<ContentItem>();
        foreach (GameObject point in Points)
        {
            _tmpContent.Add(point.GetComponent<ProjectItem>().content);
        }
        return _tmpContent;
    }

    public void SetPinsScale(float _i)
    {
        foreach (GameObject point in Points)
        {
            if (point.GetComponent<PointItem>())
            {
                point.GetComponent<PointItem>().PinScale(_i);
            }
        }
    }

    public List<int> GetProjectsList() { return projectsList; }
}

public enum CountryType
{
    Simple,
    Multyple
}