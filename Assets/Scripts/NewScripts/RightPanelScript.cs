﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RightPanelScript : MonoBehaviour
{
    static public RightPanelScript instance = null;

    public Image Logo;
    public GameObject MainImageContainer;
    public Image MainImage;
    public Button ShowHideMainImageBtn;
    public Button InfoBtn;
    public Button VideoBtn;
    public Button IntariorBtn;
    public Button ExteriorBtn;
    public Button FloorBtn;
    public Button BrochureBtn;
    public Button CloseBtn;
    public Button CloseVideoBtn;
    public VideoPlayerScript videoPlayer;
    public GameObject TextPanel;
    public Text PinDescriptionText;
    public Animator InfoTextAnimator;
    //public Animation InfoTextAnimation;
         
    //public string Path;

    Vector2 startPos;
    Vector2 mainImageContainerSize;
    bool menuIsShowing = false;
    bool mainImageIsShow = false;

    bool infoPanelIsShow = false;
    bool intariorIsShow = false;
    bool exteriorIsShow = false;
    bool floorlIsShow = false;

    bool startAnimation = false;
    Animator mainAnimator;
    Dictionary<string, List<string>> Data = new Dictionary<string, List<string>>();
    int DescriptionTextIndex = 0;
    int LanguageIndex = 0;

    void Awake()
    {
        if (instance == null)
            instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
        mainAnimator = gameObject.GetComponent<Animator>();
        mainImageContainerSize = MainImageContainer.GetComponent<RectTransform>().sizeDelta;
        startPos = gameObject.GetComponent<RectTransform>().anchoredPosition;
        ShowHideMainImageBtn.onClick.AddListener(() => ShowHideMainImage());
        InfoBtn.onClick.AddListener(() => {
            if (!infoPanelIsShow) { SelectBtn(InfoBtn); }
            else { UnSelectBtn(InfoBtn); }
            ShowTextPanel();
        });
        VideoBtn.onClick.AddListener(() => { ShowVideoPlayer(); SelectBtn(VideoBtn); });
        IntariorBtn.onClick.AddListener(() => {
            if (!intariorIsShow)
            {
                SetImageContent("interior", SliderType.Interior);
                SelectBtn(IntariorBtn);
                intariorIsShow = true;
            }
            else
            {
                UIManager.instance.RemoveSlider(SliderType.Interior);
                UnSelectBtn(IntariorBtn);
                intariorIsShow = false;
            }
        });
        ExteriorBtn.onClick.AddListener(() => {
            if (!exteriorIsShow)
            {
                SetImageContent("exterior", SliderType.Exterior); SelectBtn(ExteriorBtn);
                exteriorIsShow = true;
            }
            else
            {
                UIManager.instance.RemoveSlider(SliderType.Exterior);
                UnSelectBtn(ExteriorBtn);
                exteriorIsShow = false;
            }
        });
        FloorBtn.onClick.AddListener(() => {
            if (!floorlIsShow)
            {
                SetImageContent("plan", SliderType.Floorplan); SelectBtn(FloorBtn);
                floorlIsShow = true;
            }
            else
            {
                UIManager.instance.RemoveSlider(SliderType.Floorplan);
                UnSelectBtn(FloorBtn);
                floorlIsShow = false;
            }
        });
        BrochureBtn.onClick.AddListener(() => {
            if (Data["brochure"].Count > 0)
            {
                /*System.IO.StreamReader reader = new System.IO.StreamReader(Data["brochure"][0], System.Text.Encoding.GetEncoding(1252));
                string link = reader.ReadToEnd();
                reader.Close();*/
                Application.OpenURL(Data["brochure"][0]);
            }
        });
        CloseBtn.onClick.AddListener(() => ResetState());
        CloseVideoBtn.onClick.AddListener(() => CloseVideoPlayer());
    }

    // Update is called once per frame
    void Update()
    {
        if (LanguageIndex != FileManager.instance.LanguageIndex)
        {
            LanguageIndex = FileManager.instance.LanguageIndex;
            string Desc = FileManager.instance.GetExceleRow(LanguageIndex, DescriptionTextIndex).Discription;
            SetDexcription(PinDescriptionText, Desc);
        }
    }

    void SetImageContent(string _type, SliderType _sliderType)
    {
        if (Data[_type].Count > 0)
        {
            UIManager.instance.AddImageSlider(Data[_type], _sliderType);
        }
    }

    public void ShowHideMenu(string _path, int _descriptionIndex)
    {
        DescriptionTextIndex = _descriptionIndex;
        Data = FileManager.instance.GetDataForPin(_path);

        try
        {
            if (Data["video"].Count > 0)
            {
                videoPlayer.LoadVideo(Data["video"][0]);
            }
        }
        catch(Exception ex)
        {
            Debug.Log("Video key not found : " + ex.Message);
        }
        
        StartCoroutine(ShowPanel());
    }

    IEnumerator ShowPanel()
    {
        if (!startAnimation)
        {
            startAnimation = true;
            if (!menuIsShowing)
            {
                SetContent();

                mainAnimator.SetFloat("Direction", 1f);
                mainAnimator.Play("RightPanelShowHide", -1, 0f);
                menuIsShowing = true;
            }
            else
            {
                mainAnimator.SetFloat("Direction", -1f);
                mainAnimator.Play("RightPanelShowHide", -1, 0.8f);
                if (mainImageIsShow) { ShowHideMainImage(); }

                if (intariorIsShow) {
                    UIManager.instance.RemoveSlider(SliderType.Interior);
                    UnSelectBtn(IntariorBtn);
                    intariorIsShow = false;
                }

                if (exteriorIsShow)
                {
                    UIManager.instance.RemoveSlider(SliderType.Exterior);
                    UnSelectBtn(ExteriorBtn);
                    exteriorIsShow = false;
                }

                if (floorlIsShow)
                {
                    UIManager.instance.RemoveSlider(SliderType.Floorplan);
                    UnSelectBtn(FloorBtn);
                    floorlIsShow = false;
                }

                yield return new WaitForSeconds(1f);
                SetContent();

                mainAnimator.SetFloat("Direction", 1f);
                mainAnimator.Play("RightPanelShowHide", -1, -0.5f);
            }
            Loom.QueueOnMainThread(() => {
                startAnimation = false;
            }, 1f);
        }
    }

    void SetContent()
    {
        try
        {
            if (Data["logo"].Count > 0) { UIManager.instance.SetImageFromFile(Logo, Data["logo"][0]); }
            if (Data["masterPic"].Count > 0) { UIManager.instance.CorrectImageSize(MainImage, Data["masterPic"][0]); }
            if (Data["info"].Count > 0) { SetDescText(PinDescriptionText, Data["info"][0]); }

            string Desc = FileManager.instance.GetExceleRow(LanguageIndex, DescriptionTextIndex).Discription;
            string BrochureURL = FileManager.instance.GetBroshureByProjectId(DescriptionTextIndex).URLofBrochure;
            SetDexcription(PinDescriptionText, Desc);
            if (BrochureURL.Trim() != "")
            {
                Data["brochure"].Add(BrochureURL.Trim());
            }

            if (Data["interior"].Count > 0) { IntariorBtn.gameObject.SetActive(true); }
            else { IntariorBtn.gameObject.SetActive(false); }

            if (Data["exterior"].Count > 0) { ExteriorBtn.gameObject.SetActive(true); }
            else { ExteriorBtn.gameObject.SetActive(false); }

            if (Data["plan"].Count > 0) { FloorBtn.gameObject.SetActive(true); }
            else { FloorBtn.gameObject.SetActive(false); }

            if (Data["brochure"].Count > 0) { BrochureBtn.gameObject.SetActive(true); }
            else { BrochureBtn.gameObject.SetActive(false); }
        }
        catch(Exception ex)
        {
            Debug.Log(ex.Message);
        }
        
    }



    public void HideMenu()
    {
        //StartCoroutine(HidePanel());
        if (!startAnimation)
        {
            startAnimation = true;
            mainAnimator.SetFloat("Direction", -1f);
            mainAnimator.Play("RightPanelShowHide", -1, 0.8f);
            menuIsShowing = false;
            UnSelectBtns();
            Loom.QueueOnMainThread(() => {
                startAnimation = false;
            }, 1f);
        }
        
    }

    IEnumerator HidePanel()
    {
        for (float i = 0; i < 1.1; i += 0.05f)
        {
            gameObject.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(Vector2.zero, startPos, i);
            yield return new WaitForSeconds(0.01f);
        }
    }

    void ShowHideMainImage()
    {
        StartCoroutine(ShowHideMainImageAnimation());
    }

    IEnumerator ShowHideMainImageAnimation()
    {
        Vector2 startSize, endSize = Vector2.zero;
        if (!mainImageIsShow)
        {
            startSize = mainImageContainerSize;
            endSize = MainImage.gameObject.GetComponent<RectTransform>().sizeDelta;
            ShowHideMainImageBtn.gameObject.transform.GetChild(0).GetComponent<RectTransform>().rotation = Quaternion.identity;
            mainImageIsShow = true;
        }
        else
        {
            startSize = MainImage.gameObject.GetComponent<RectTransform>().sizeDelta;
            endSize = mainImageContainerSize;
            ShowHideMainImageBtn.gameObject.transform.GetChild(0).GetComponent<RectTransform>().rotation = new Quaternion(0, 0, 180, 0);
            mainImageIsShow = false;
        }
        for (float i = 0; i < 1.1; i += 0.05f)
        {
            MainImageContainer.GetComponent<RectTransform>().sizeDelta = Vector2.Lerp(startSize, endSize, i);
            yield return new WaitForSeconds(0.01f);
        }
    }

    public void ResetState()
    {
        if (mainImageIsShow) { ShowHideMainImage(); }
        if (menuIsShowing) { HideMenu(); }
        if (infoPanelIsShow) { UnSelectBtn(InfoBtn); ShowTextPanel(); }

        if (intariorIsShow) {
            UIManager.instance.RemoveSlider(SliderType.Interior);
            UnSelectBtn(IntariorBtn);
            intariorIsShow = false;
        }
        if (exteriorIsShow)
        {
            UIManager.instance.RemoveSlider(SliderType.Exterior);
            UnSelectBtn(ExteriorBtn);
            exteriorIsShow = false;
        }
        if (floorlIsShow)
        {
            UIManager.instance.RemoveSlider(SliderType.Floorplan);
            UnSelectBtn(FloorBtn);
            floorlIsShow = false;
        }
    }

    void CloseVideoPlayer()
    {
        videoPlayer.Stop();
        videoPlayer.gameObject.SetActive(false);
        UnSelectBtn(VideoBtn);
    }

    void ShowVideoPlayer()
    {
        if (intariorIsShow)
        {
            UIManager.instance.RemoveSlider(SliderType.Interior);
            UnSelectBtn(IntariorBtn);
            intariorIsShow = false;
        }
        if (exteriorIsShow)
        {
            UIManager.instance.RemoveSlider(SliderType.Exterior);
            UnSelectBtn(ExteriorBtn);
            exteriorIsShow = false;
        }
        if (floorlIsShow)
        {
            UIManager.instance.RemoveSlider(SliderType.Floorplan);
            UnSelectBtn(FloorBtn);
            floorlIsShow = false;
        }
        videoPlayer.gameObject.SetActive(true);
        videoPlayer.Play();
    }

    void ShowTextPanel()
    {
        if (!infoPanelIsShow)
        {
            InfoTextAnimator.SetFloat("Direction", 1);
            InfoTextAnimator.Play("ShowInfoPanel", -1, 0f);
            infoPanelIsShow = true;
        }
        else
        {
            InfoTextAnimator.SetFloat("Direction", -1);
            InfoTextAnimator.Play("ShowInfoPanel", -1, 1f);
            infoPanelIsShow = false;
        }
        
    }

    void SetDescText(Text txt, string file)
    {
        System.IO.StreamReader reader = new System.IO.StreamReader(file, System.Text.Encoding.GetEncoding(1252));
        string text = reader.ReadToEnd();
        reader.Close();
        txt.text = text;
        Vector2 newSize = new Vector2(630, txt.preferredHeight + 60);
        txt.transform.parent.GetComponent<RectTransform>().sizeDelta = newSize;
    }

    void SetDexcription(Text _txt, string desc)
    {
        _txt.text = desc;
        Vector2 newSize = new Vector2(630, _txt.preferredHeight + 60);
        _txt.transform.parent.GetComponent<RectTransform>().sizeDelta = newSize;
    }

    void UnSelectBtns()
    {
        Color normalColor = new Color(1,1,1, 0.4f);
        InfoBtn.GetComponent<Image>().color = normalColor;
        VideoBtn.GetComponent<Image>().color = normalColor;
        IntariorBtn.GetComponent<Image>().color = normalColor;
        ExteriorBtn.GetComponent<Image>().color = normalColor;
        FloorBtn.GetComponent<Image>().color = normalColor;
    }


    void SelectBtn(Button _btn)
    {
        //UnSelectBtns();
        Color selectlColor = new Color(1, 1, 1, 1);
        _btn.GetComponent<Image>().color = selectlColor;
    }

    void UnSelectBtn(Button _btn)
    {
        Color normalColor = new Color(1, 1, 1, 0.4f);
        _btn.GetComponent<Image>().color = normalColor;
    }

    public void UnSelectBtn(SliderType _sliderType)
    {
        switch (_sliderType)
        {
            case SliderType.Interior:
                UnSelectBtn(IntariorBtn);
                intariorIsShow = false;
                break;
            case SliderType.Exterior:
                UnSelectBtn(ExteriorBtn);
                exteriorIsShow = false;
                break;
            case SliderType.Floorplan:
                UnSelectBtn(FloorBtn);
                floorlIsShow = false;
                break;
        }
    }
}