﻿using Lean.Touch;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondCamera : MonoBehaviour
{

    static public SecondCamera instance = null;

    public CountryScript[] Countries;
    public GameObject Target;
    public float speed;
    public GameObject Pawn;

    [Range(0, 1)]
    public float SmoothValue;
    public float SmoothRotationVal;
    public bool CanSmoothMoving = true;

    Vector3 pos;
    Vector3 startPos;
    Vector3 lastPos;
    Vector3 lastLook;
    public Vector3 lookTarget;
    Quaternion startLook;
    Vector3 startLookAngle;
    public bool canLook = false;
    bool canHit = false;
    int CountryIndex = 0;
    float temps;
    CityScript _city;

    void Awake()
    {
        if (instance == null)
            instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
        startLook = transform.rotation;
        startLookAngle = transform.eulerAngles;
    }

    // Update is called once per frame
    void Update()
    {

        if (canHit && Input.GetMouseButtonDown(0))
        {
            temps = Time.time;
        }

        if (canHit && Input.GetMouseButtonUp(0))
        {
            // short Click
            if ((Time.time - temps) < 0.2)
            {
                Ray ray = gameObject.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 100))
                {
                    if (hit.collider.gameObject.GetComponent<CityScript>())
                    {
                        _city = hit.collider.gameObject.GetComponent<CityScript>();

                        if (CountryIndex == 0)
                        {
                            Loom.QueueOnMainThread(() => {
                                ResetMovingState(true, true);
                            }, .6f);
                        }

                        if (_city.ShowWhayDamacPanel) {
                            WhyDubaiDamacScript.instance.ShowContent();
                        }

                        UIManager.instance.PutLegendBtn(_city.CityExcelId, ExcelParam.City, () => {
                            ThirdCamera.instance.ResetState();
                            FadeOutScreen.instance.StartFade("fromThirdToSecond", 0f);
                            MoveBackToLastPos(0.5f);
                            CanMovingAndScaling();
                            _city.ShowPoints(0);
                        });
                        iTween.MoveTo(Pawn, iTween.Hash("position", _city.EndTarget.transform, "time", .6f, "delay", 0f, "easetype", iTween.EaseType.easeOutQuad));
                        iTween.RotateTo(Pawn, iTween.Hash("rotation", _city.EndTarget.transform.eulerAngles, "time", .6f, "delay", 0f, "easetype", iTween.EaseType.easeOutSine));
                        lookTarget = _city.EndTarget.transform.GetChild(0).transform.position;

                        _city.ShowPoints(0.65f);
                        canHit = false;
                    }
                }
            }
        }
    }

    float smoothTime = 0.3f;
    float xVelocity , yVelocity, zVelocity = 0.0f;

    private void LateUpdate()
    {
        transform.rotation = Pawn.transform.rotation;
        if (CanSmoothMoving)
        {
            //transform.position = SmoothMove(transform.position, Pawn.transform.position, SmoothValue);

            float newPositionX = Mathf.SmoothDamp(transform.position.x, Pawn.transform.position.x, ref xVelocity, smoothTime);
            float newPositionY = Mathf.SmoothDamp(transform.position.y, Pawn.transform.position.y, ref yVelocity, smoothTime);
            float newPositionZ = Mathf.SmoothDamp(transform.position.z, Pawn.transform.position.z, ref zVelocity, smoothTime);
            transform.position = new Vector3(newPositionX, newPositionY, newPositionZ);

        }
        else {
            transform.position = Pawn.transform.position;
        }


        if (canLook)
        {
            transform.LookAt(lookTarget);
        }
    }

    Vector3 SmoothMove(Vector3 _lastpos, Vector3 _pos, float _c)
    {
        return (_pos - _lastpos) * _c + _lastpos;
        //transform.position = Vector3.Lerp(transform.position, _pos, 30 * Time.deltaTime);
    }

    void SmoothRotation(Vector3 _rotation, float _c)
    {
        //Debug.Log(transform.eulerAngles);
        transform.eulerAngles = (_rotation - transform.eulerAngles) * _c + transform.eulerAngles;

    }

    

    public void StartMove(float _time, float _delay, float _fadeOutDelay, int _countryIndex)
    {
        HideCountries();
        CountryIndex = _countryIndex;
        Countries[CountryIndex].gameObject.SetActive(true);
        if (Countries[CountryIndex].Citys.Length == 1)
        {
            Target = Countries[CountryIndex].EndTarget;
            lookTarget = Countries[CountryIndex].Citys[0].Points[0].transform.position;

            canLook = true;
        }
        else
        {
            Target = Countries[CountryIndex].StartTarget;
            transform.rotation = startLook;
            canLook = false;
            iTween.RotateTo(Pawn, iTween.Hash("rotation", Target.transform.eulerAngles, "time", _time, "delay", _delay, "easetype", iTween.EaseType.easeOutSine));
        }
        iTween.MoveTo(Pawn, iTween.Hash("position", Target.transform, "time", _time, "delay", _delay, "easetype", iTween.EaseType.easeOutQuad));
        
        //ResetMovingState(false, false);
        StartCoroutine(Prepare());
        FadeOutScreen.instance.StartFade("fromFirstToSecond", _fadeOutDelay);
    }
    
    public void SaveLastPos() { lastPos = transform.position; lastLook = transform.rotation.eulerAngles; }

    public void MoveToTargetPos(float _delay)
    {
        iTween.MoveTo(Pawn, iTween.Hash("position", Target.transform, "time", 1.5f, "delay", _delay, "easetype", iTween.EaseType.easeOutSine));
        iTween.RotateTo(Pawn, iTween.Hash("rotation", Target.transform.eulerAngles, "time", 1.5f, "delay", _delay, "easetype", iTween.EaseType.easeOutSine));
        Loom.QueueOnMainThread(() => {
            canHit = true;
        }, 1.5f);
    }

    public void MoveToStartPos(float _delay)
    {
        canHit = false;
        iTween.MoveTo(Pawn, iTween.Hash("position", startPos, "time", 1.5f, "delay", _delay, "easetype", iTween.EaseType.easeInOutCubic));
        iTween.RotateTo(Pawn, iTween.Hash("rotation", startLookAngle, "time", 1.5f, "delay", _delay, "easetype", iTween.EaseType.easeInOutCubic));
        Loom.QueueOnMainThread(() => {
            Pawn.GetComponent<LeanCameraZoom>().Zoom = 0;
            //gameObject.GetComponent<Camera>().fieldOfView = 60;
        }, 1.5f);
    }

    public void MoveBackToLastPos(float _delay)
    {
        iTween.MoveTo(Pawn, iTween.Hash("position", lastPos, "time", 1.5f, "delay", _delay, "easetype", iTween.EaseType.easeOutSine));
        iTween.RotateTo(Pawn, iTween.Hash("rotation", lastLook, "time", 1.5f, "delay", _delay, "easetype", iTween.EaseType.easeOutSine));
    }

    

    IEnumerator Prepare()
    {
        yield return new WaitForSeconds(1.5f);
        //canLook = false;
        if (Countries[CountryIndex].Citys.Length > 1)
        {
            canHit = true;
            canLook = false;
        }
    }

    IEnumerator Look(Transform me, Vector3 target, float t)
    {
        for (float i = 0; i < 1.1f; i += 0.01f)
        {
            yield return new WaitForSeconds(0.01f);
            var look = Quaternion.LookRotation(target - me.position);
            me.rotation = Quaternion.Lerp(me.rotation, look, i);
            //me.rotation = Quaternion.Lerp(me.rotation, look, i * Time.deltaTime);
        }
        
    }

    public void MoveTo(Vector3 _pos, float _time, float _delay)
    {
        iTween.MoveTo(Pawn, iTween.Hash("position", _pos, "time", _time, "delay", _delay, "easetype", iTween.EaseType.easeOutQuint));
        iTween.LookTo(gameObject, iTween.Hash("looktarget", _pos, "time", _time + 0.5f, "delay", _delay, "easetype", iTween.EaseType.easeOutQuint));

        if (CountryIndex == 0)
        {
            ResetMovingState(false, false);
            _city.HidePoints(0);
            WhyDubaiDamacScript.instance.CloseContent();
        }
    }

    void HideCountries()
    {
        foreach (CountryScript country in Countries)
        {
            country.gameObject.SetActive(false);
        }
    }

    public CountryScript GetCurrentCountry()
    {
        return Countries[CountryIndex];
    }

    public void UserCanHit() { canHit = true; }
    public void HideCountryItems()
    {
        if (_city)
        {
            _city.HidePoints(0.3f);
            //Countries[CountryIndex].ShowPointsAnimation();
        }
    }
    public void CanMovingAndScaling()
    {
        if (CountryIndex == 0)
        {
            ResetMovingState(true, false);
            WhyDubaiDamacScript.instance.ShowContent();
        }
    }

    public void ResetState(bool _canMovingAndScaling, bool _resetCanHit = false)
    {
        if (CountryIndex == 0)
        {
            ResetMovingState(false, false);

        }
        if (_city != null && _city.ShowWhayDamacPanel) {
            WhyDubaiDamacScript.instance.CloseContent();
        }

        HideCountryItems();
    }

    public void ResetMovingState(bool _state, bool _updateMovement)
    {
        Pawn.GetComponent<LeanCameraMove>().SetCanMove(_state);
        Pawn.GetComponent<LeanCameraZoom>().canUse = _state;
        Pawn.GetComponent<RotateCameraAroundHit>().canUse = _state;
        Pawn.GetComponent<LimitCameraMoving>().SetStartPos(_state, _updateMovement);
        
    }
}

static class Extensions
{
    public static void LookAtSmooth(this Transform me, Vector3 target, float t)
    {
        var look = Quaternion.LookRotation(target - me.position);

        me.rotation = Quaternion.Lerp(me.rotation, look, t * Time.deltaTime);
    }
}
