﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FavoriteRow : MonoBehaviour
{
    //public Text Number;
    public Text Title;
    public Button Btn;
    public Button RemoveBtn;
    public ContentItem content;

    Animator animator;

    int LanguageIndex = 0;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(gameObject.GetComponent<RectTransform>().sizeDelta.x, 0);
        animator = GetComponent<Animator>();
        RemoveBtn.onClick.AddListener(() => RemoveFromFavorite());
        animator.SetFloat("Direction", -1);
        animator.Play("ShowHideFavouriteRow", -1, 1);
    }

    // Update is called once per frame
    void Update()
    {
        if (LanguageIndex != FileManager.instance.LanguageIndex)
        {
            LanguageIndex = FileManager.instance.LanguageIndex;
            SetNameText();
        }
    }

    

    public void SetNameText()
    {
        string _tmpName = FileManager.instance.GetExceleRow(LanguageIndex, content.ExelIndexName).Project;
        _tmpName = _tmpName.Replace("<En>", "");
        _tmpName = _tmpName.Replace("</En>", " ");
        Title.text = _tmpName;
    }

    public void SetData(ContentItem _content)
    {
        //Number.text = _number;
        //Title.text = _content.ItemName;
        content = _content;
        SetNameText();
    }

    public void RemoveFromFavorite()
    {
        FavoritelistScript.instance.RemoveFavorite(content);
        LeftPanelScript.instance.UnSelectFavoriteInList(content);
        DestroyObj();
    }

    public void DestroyObj() {
        animator.SetFloat("Direction", 1);
        animator.Play("ShowHideFavouriteRow", -1, 0);
        Destroy(gameObject, 1f);
    }
}
