﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountryItem : MonoBehaviour
{
    public GameObject FocusCamera;
    public GameObject btnObj;
    public Button btn;
    public int CountryIndex;
    public int CountryExcelId;
    public Vector2 clickTarget;
    public CameraOrbit cameraOrbit;
    public bool NeedShowProjectList = true;

    //Entity_English.CountryParam countryParam = null;
    int LanguageIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        btn.onClick.AddListener(() => GoToLevelTwo());
        InitCountryData();
    }

    void Update()
    {
        if (LanguageIndex != FileManager.instance.LanguageIndex)
        {
            LanguageIndex = FileManager.instance.LanguageIndex;
            SetNameText();
        }
    }

    void InitCountryData() {
        SecondCamera.instance.Countries[CountryIndex].SetCountryData(CountryExcelId);
        SetNameText();
    }

    public void SetNameText()
    {
        //countryParam = FileManager.instance.GetCountryParam(LanguageIndex, CountryExcelId);
        SecondCamera.instance.Countries[CountryIndex].UpdateLanguage();
        string _tmpName = SecondCamera.instance.Countries[CountryIndex].countryParam.Country;
        _tmpName = _tmpName.Replace("<En>", "");
        _tmpName = _tmpName.Replace("</En>", " ");
        btn.gameObject.GetComponentInChildren<Text>().text = _tmpName;
        Vector2 newSize = new Vector2(btn.gameObject.GetComponentInChildren<Text>().preferredWidth, 25);
        newSize.x = newSize.x + 20;
        btn.transform.parent.GetComponent<RectTransform>().sizeDelta = newSize;
    }

    private void LateUpdate()
    {
        //btnObj.transform.LookAt(FocusCamera.transform);
        btnObj.transform.LookAt(btnObj.transform.position + FocusCamera.transform.rotation * Vector3.forward, FocusCamera.transform.rotation * Vector3.up);
    }

    public void GoToLevelTwo()
    {
        if (!FirstCamera.instance.chooseCountry)
        {
            FirstCamera.instance.MovingToCountry(transform, clickTarget);
            SecondCamera.instance.StartMove(0.75f, 1.2f, 1.1f, CountryIndex);
            //string ChineseName = FileManager.instance.GetChiniseText(ChineseNameIndex).Country;
            if (NeedShowProjectList) {
                Loom.QueueOnMainThread(() => {
                    LeftPanelScript.instance.SetProjectList(SecondCamera.instance.Countries[CountryIndex].Citys[0].GetProjectsList(), false);
                    LeftPanelScript.instance.ShowProjectList(1.95f);
                }, 0.1f);
            }
            UIManager.instance.PutLegendBtn(CountryExcelId, ExcelParam.Country, () => {

                if (FadeOutScreen.instance.levelIndex == 3)
                {
                    ThirdCamera.instance.ResetState();
                    FadeOutScreen.instance.StartFade("fromThirdToSecond", 0.25f);
                    SecondCamera.instance.MoveToTargetPos(0.5f);
                    //SecondCamera.instance.ResetState(true, true);
                }
                else if (FadeOutScreen.instance.levelIndex == 2)
                {
                    SecondCamera.instance.MoveToTargetPos(0);
                    RightPanelScript.instance.ResetState();
                }
                SecondCamera.instance.ResetState(true);
            });


        }
    }
}
