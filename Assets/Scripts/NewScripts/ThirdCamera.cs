﻿using Lean.Touch;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdCamera : MonoBehaviour
{
    static public ThirdCamera instance = null;

    public bool canMoveing;
    public GameObject[] Areas;
    Vector3 startPos;
    Vector3 startLook;
    //Quaternion startLook;
    int AreaIndex = 0;
    public bool choosArea = false;
    public GameObject Pawn;

    [Range(0, 1)]
    public float SmoothValue;

    void Awake()
    {
        if (instance == null)
            instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
        //startLook = transform.rotation;
        //startLook = new Vector3(transform.rotation.x, transform.rotation.y, transform.rotation.z);
        startLook = transform.eulerAngles;
        //Debug.Log("startLook: " + startLook);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void LateUpdate()
    {
        //transform.position = Pawn.transform.position;
        transform.rotation = Pawn.transform.rotation;
        transform.position = SmoothMove(transform.position, Pawn.transform.position, SmoothValue);
        //SmoothRotation(Pawn.transform.eulerAngles, SmoothValue);
    }

    Vector3 SmoothMove(Vector3 _lastpos, Vector3 _pos, float _c)
    {
        return (_pos - _lastpos) * _c + _lastpos;
        //transform.position = Vector3.Lerp(transform.position, _pos, 30 * Time.deltaTime);
    }

    void SmoothRotation(Vector3 _rotation, float _c)
    {
        if (transform.eulerAngles.y != 0)
        {
            transform.eulerAngles = (_rotation - transform.eulerAngles) * _c + transform.eulerAngles;
        }

    }

    void SmoothRotation(Quaternion _rotation, float _c)
    {
        //transform.rotation = Quaternion.RotateTowards(transform.rotation, _rotation, 30 * Time.deltaTime);
        transform.rotation = Quaternion.Lerp(transform.rotation, _rotation, 30 * Time.deltaTime);
    }

    public void StartMove(float _time, float _delay, float _fadeOutDelay)
    {

        iTween.MoveTo(Pawn, iTween.Hash("position", Areas[AreaIndex].GetComponent<AreaScript>().EndTarget.transform, "time", _time, "delay", _delay, "easetype", iTween.EaseType.easeOutQuint));
        iTween.RotateTo(Pawn, iTween.Hash("rotation", Areas[AreaIndex].GetComponent<AreaScript>().EndTarget.transform, "time", _time, "delay", _delay, "easetype", iTween.EaseType.easeOutQuint));
        FadeOutScreen.instance.StartFade("fromSecondToThird", _fadeOutDelay);
        LeftPanelScript.instance.ShowProjectList(_delay);
        Loom.QueueOnMainThread(() => {
            Pawn.GetComponent<LeanCameraMove>().SetCanMove(true);
            Pawn.GetComponent<LeanCameraZoom>().canUse = true;
            Pawn.GetComponent<RotateCameraAroundHit>().canUse = true;
            Pawn.GetComponent<LimitCameraMoving>().SetStartPos(true, true);
        }, _time);
    }

    public void MoveBack(float _delay)
    {
        iTween.MoveTo(Pawn, iTween.Hash("position", startPos, "time", 1.5f, "delay", _delay, "easetype", iTween.EaseType.easeOutSine));
        iTween.RotateTo(Pawn, iTween.Hash("rotation", Areas[AreaIndex].GetComponent<AreaScript>().EndTarget.transform, "time", 1.5f, "delay", _delay, "easetype", iTween.EaseType.easeOutSine));
        Loom.QueueOnMainThread(() => {
            Areas[AreaIndex].GetComponent<AreaScript>().SetStartRotation();
        }, _delay + 1.5f);
    }

    public void SetActiveAreaByIndex(int _index)
    {
        HideAllAreas();
        AreaIndex = _index;
        Areas[AreaIndex].SetActive(true);
        
        Pawn.GetComponent<LimitCameraMoving>().xLimit = Areas[AreaIndex].GetComponent<AreaScript>().XLimit;
        Pawn.GetComponent<LimitCameraMoving>().yLimit = Areas[AreaIndex].GetComponent<AreaScript>().YLimit;
        Pawn.GetComponent<LimitCameraMoving>().zLimit = Areas[AreaIndex].GetComponent<AreaScript>().ZLimit;
        Pawn.GetComponent<LeanCameraZoom>().ZoomMin = Areas[AreaIndex].GetComponent<AreaScript>().ZoomLimit.x;
        Pawn.GetComponent<LeanCameraZoom>().ZoomMax = Areas[AreaIndex].GetComponent<AreaScript>().ZoomLimit.y;
        Pawn.GetComponent<LeanCameraZoom>().UpdateMovingLimit();
        choosArea = true;
        Loom.QueueOnMainThread(() => {
            Areas[AreaIndex].GetComponent<AreaScript>().InitArea();
            LeftPanelScript.instance.SetProjectList(Areas[AreaIndex].GetComponent<AreaScript>().GetProjectsList(), false);
        }, 0.1f);
    }

    public void HideAllAreas()
    {
        foreach (GameObject area in Areas)
        {
            area.SetActive(false);
        }
    }

    public void ResetState()
    {
        LeftPanelScript.instance.ResetState();
        RightPanelScript.instance.ResetState();
        Pawn.GetComponent<LeanCameraMove>().SetCanMove(false);
        Pawn.GetComponent<LeanCameraZoom>().canUse = false;
        Pawn.GetComponent<RotateCameraAroundHit>().canUse = false;
        Pawn.GetComponent<LimitCameraMoving>().SetStartPos(false, false);
        MoveBack(0);
        choosArea = false;
        Loom.QueueOnMainThread(() => {
            Pawn.GetComponent<LeanCameraZoom>().Zoom = 0;
            //gameObject.GetComponent<Camera>().fieldOfView = 60;
        }, 1.2f);
    }

    public AreaScript GetCurrentArea()
    {
        return Areas[AreaIndex].GetComponent<AreaScript>();
    }
}
