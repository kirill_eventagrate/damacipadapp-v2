﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LanguageBtn : MonoBehaviour
{
    public int index;
    public string LanguageName;
    public Text LanguageText;
    Button btn;
    // Start is called before the first frame update
    void Start()
    {
        if (GetComponent<Button>())
        {
            btn = GetComponent<Button>();
            btn.onClick.AddListener(() => {
                ClickOnBtn();
            });
        }
            
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetData(string _languageName, int _index)
    {
        LanguageName = _languageName;
        index = _index;
        LanguageText.text = LanguageName;
    }

    void ClickOnBtn()
    {
        FileManager.instance.LanguageIndex = index;
        SettingPanelScript.instance.SelectBtn(btn);
        SettingPanelScript.instance.ShowHidePanel();
    } 

}
