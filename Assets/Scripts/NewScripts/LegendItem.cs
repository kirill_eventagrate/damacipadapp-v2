﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LegendItem : MonoBehaviour
{
    Button btn;
    Action action;
    public int Index;
    public List<string> Names;
    public int ExcelRowIndex = 2;
    //public Entity_English.Param Param;
    public ExcelParam excelParam;
    int LanguageIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        btn = gameObject.GetComponent<Button>();
        btn.onClick.AddListener(() => ClickOnLegendBtn());
        Index = UIManager.instance.GetLegendItemsCount() - 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (LanguageIndex != FileManager.instance.LanguageIndex)
        {
            LanguageIndex = FileManager.instance.LanguageIndex;
            SetNameText();
        }
    }

    public void SetNameText()
    {
        string _tmpName = "";  // FileManager.instance.GetExceleRow(LanguageIndex, ExcelRowIndex)
        switch (excelParam)
        {
            case ExcelParam.ProjectId:
                _tmpName = Names[LanguageIndex];//FileManager.instance.GetExceleRow(LanguageIndex, ExcelRowIndex).ProjectId;
                break;
            case ExcelParam.Country:
                _tmpName = FileManager.instance.GetCountryParam(LanguageIndex, ExcelRowIndex).Country;
                break;
            case ExcelParam.City:
                _tmpName = FileManager.instance.GetCityParam(LanguageIndex, ExcelRowIndex).City;
                break;
            case ExcelParam.Area:
                _tmpName = FileManager.instance.GetAreaParam(LanguageIndex, ExcelRowIndex).Area;
                break;
            case ExcelParam.Project:
                _tmpName = FileManager.instance.GetExceleRow(LanguageIndex, ExcelRowIndex).Project;
                break;
        }
        _tmpName = _tmpName.Replace("<En>", "");
        _tmpName = _tmpName.Replace("</En>", " ");
        _tmpName = _tmpName.ToUpper();
        GetComponentInChildren<Text>().text = _tmpName;
        Vector2 newSize = new Vector2(transform.GetChild(1).gameObject.GetComponent<Text>().preferredWidth + 50, 100);
        gameObject.GetComponent<RectTransform>().sizeDelta = newSize;
    }

    public void SetAction(Action _action)
    {
        action = _action;
    }

    public void ClickOnLegendBtn()
    {
        try
        {
            ((Action)action)();
            UIManager.instance.RemoveLastItem(Index);
        }
        catch
        {
        }
    }

    public void SetEnableState(bool _state)
    {
        gameObject.GetComponent<Button>().enabled = _state;
        if (_state)
        {
            GetComponentInChildren<Text>().color = Color.white;
        }
        else
        {
            Color disableColor = new Color(0.7803922f, 0.6901961f, 0.4431373f);
            GetComponentInChildren<Text>().color = disableColor;
        }
    }

    
}
