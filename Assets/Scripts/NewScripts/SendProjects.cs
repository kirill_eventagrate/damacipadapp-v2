﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class SendProjects : MonoBehaviour
{

    public InputField NameInputField;
    public InputField EmailInputField;
    public InputField PhoneInputField;
    public Button CancelBtn;
    public Button SendBtn;

    // Start is called before the first frame update
    void Start()
    {
        CancelBtn.onClick.AddListener(() => Cancel());
        SendBtn.onClick.AddListener(() => Send());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Cancel()
    {
        ClearInputs();
        ClosePanel();
    }

    void Send()
    {
        ClearInputs();
        ClosePanel();

        if (EmailInputField.text.Trim() != "")
        {
            ThreadStart threadStart = new ThreadStart(SendMail);
            Thread thread = new Thread(threadStart);
            thread.IsBackground = true;
            thread.Start();
        }

        
    }

    void ClosePanel()
    {
        gameObject.SetActive(false);
    }

    void ClearInputs()
    {
        NameInputField.text = "";
        EmailInputField.text = "";
        PhoneInputField.text = "";
    }

    private void SendMail()
    {
        //smtp сервер 
        string smtpHost = "smtp.sendgrid.net";
        //smtp порт 
        int smtpPort = 587;
        //логин 
        string login = "eventagrate";
        //пароль 
        string pass = "Eventagrate1";
        
        //создаем подключение
        SmtpClient client = new SmtpClient(smtpHost, smtpPort);

        client.Credentials = ((System.Net.ICredentialsByHost)(new NetworkCredential(login, pass)));

        //От кого письмо
        string from = "kirill@eventagrate.com";
        //Кому письмо
        string to = EmailInputField.text;
        //Тема письма
        string subject = "Project list";
        //Текст письма
        string body = "Hello " + NameInputField.text + "! \n\n ";
        body += "Your Project list: \n\n";
        foreach (ContentItem content in FavoritelistScript.instance.Favoritelist)
        {
            string _tmpName = FileManager.instance.GetExceleRow(FileManager.instance.LanguageIndex, content.ExelIndexName).Project;
            body += _tmpName + "\n";
        }
        //Создаем сообщение
        MailMessage mess = new MailMessage(from, to, subject, body);

        try
        {
            client.Send(mess);
        }
        catch (System.Exception ex)
        {
            Debug.Log(ex);
        }
    }
}
