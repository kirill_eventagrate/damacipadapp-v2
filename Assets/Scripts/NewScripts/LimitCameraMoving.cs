﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitCameraMoving : MonoBehaviour
{
    public Vector2 xLimit;
    public Vector2 yLimit;
    public Vector2 zLimit;

    public Vector3 startPos;
    public bool canUse = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (canUse)
        {
            float x = Mathf.Clamp(transform.position.x, startPos.x + xLimit.x, startPos.x + xLimit.y);
            float y = Mathf.Clamp(transform.position.y, startPos.y + yLimit.x, startPos.y + yLimit.y);
            float z = Mathf.Clamp(transform.position.z, startPos.z + zLimit.x, startPos.z + zLimit.y);
            transform.position = new Vector3(x, y, z);
        }
    }

    public void SetStartPos(bool _state, bool _updatePos)
    {
        canUse = _state;
        if (_updatePos)
        {
            startPos = transform.position;
        }
    }

    public bool CheckHeightForZoom(float _z)
    {
        if (_z > (startPos.z + zLimit.x) && _z < (startPos.z + zLimit.y))
        {
            return true;
        }
        return false;
    }

    public Vector3 GetCurrentPos(Vector3 _pos) {
        float x = Mathf.Clamp(_pos.x, startPos.x + xLimit.x, startPos.x + xLimit.y);
        float y = Mathf.Clamp(_pos.y, startPos.y + yLimit.x, startPos.y + yLimit.y);
        float z = Mathf.Clamp(_pos.z, startPos.z + zLimit.x, startPos.z + zLimit.y);
        return new Vector3(x, y, z);
    }
}
