﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderScript : MonoBehaviour
{

    public Text[] TextField;
    public Animator ContentAnimator;

    public Slide[] _slide;

    Animator animator;

    int slideIndex = 0;
    int LanguageIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowSlider() {
        PlayAnimation(animator, "ShowSliderAnib", 1, 0);
    }

    public void HideSlider()
    {
        PlayAnimation(animator, "ShowSliderAnib", -1, 1);
    }

    void PlayAnimation(Animator _animator, string _animName, float _direction, float _startTime) {
        _animator.SetFloat("Direction", _direction);
        _animator.Play(_animName, -1, _startTime);
    }

    public void NextSlide(int _LanguageIndex)
    {
        if (slideIndex + 1 < _slide.Length)
        {
            slideIndex++;
            SetContentBySlideIndex(true);
        }
    }

    public void PrevSlide(int _LanguageIndex) {
        LanguageIndex = _LanguageIndex;
        if (slideIndex - 1 >= 0)
        {
            slideIndex--;
            SetContentBySlideIndex(true);
        }
    }

    public void InitStartContent() {
        slideIndex = 0;
        SetContentBySlideIndex(false);
    }

    public void UpdateLanguage(int _LanguageIndex) {
        LanguageIndex = _LanguageIndex;
        SetContentBySlideIndex(true);
    }

    void SetContentBySlideIndex(bool _needAnimation)
    {
        float _delay = 0;
        if (_needAnimation)
        {
            PlayAnimation(ContentAnimator, "ShowHideSliderContent", -1, 1);
            _delay = 0.25f;
        }
        Loom.QueueOnMainThread(() => {
            for (int i = 0; i < TextField.Length; i++)
            {
                TextField[i].text = _slide[slideIndex].GetCorrectLanguageArray(LanguageIndex)[i];
            }
            if (_needAnimation)
            {
                PlayAnimation(ContentAnimator, "ShowHideSliderContent", 1, 0);
            }
        }, _delay);
    }

    public int GetCorrectSlideIndex() { return slideIndex; }

}

[System.SerializableAttribute]
public class Slide {

    [TextArea]
    public string[] EnglishText;
    [TextArea]
    public string[] ChineseText;

    public Slide() { }

    public string[] GetCorrectLanguageArray(int _index) {
        if (_index == 0) {
            return EnglishText;
        }
        else {
            return ChineseText;
        }
    }
}