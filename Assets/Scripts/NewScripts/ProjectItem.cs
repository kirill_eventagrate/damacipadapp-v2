﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProjectItem : MonoBehaviour
{
    public string Name;
    public string ChineseName;
    public string Path;
    public int ExelIndexName;
    public GameObject ItemContainer;
    public Button Btn;
    public Camera lookCamera;
    public bool isCluster;
    public bool isCountryMainPin;
    public int[] ProjectsIndexes;
    public Sprite selectImg;
    public AreaScript areaScript;

    GameObject quad;
    Sprite normalImg;
    int LanguageIndex = 0;
    Entity_English.ProjectParam projectParam = null;


    public ContentItem content = new ContentItem();

    private void Awake()
    {
        normalImg = Btn.gameObject.GetComponent<Image>().sprite;
    }
    // Start is called before the first frame update
    void Start()
    {
        if (isCluster)
        {
            Btn.onClick.AddListener(() => SetFilterByCluster());
            quad = ItemContainer;
        }
        else
        { 
            Btn.onClick.AddListener(delegate {
                 content.ClickOnBtn();
                 if (!isCountryMainPin)
                 {
                     areaScript.UnSelectAllPins();
                     Select();
                 }
             });
            quad = ItemContainer.transform.GetChild(1).gameObject;

            Path = FileManager.instance.Settings["PinsFolder"][ExelIndexName - 1]["Path"].ToString();
            content.SetData(ExelIndexName);

            if (isCountryMainPin)
                SetNameText();
        }
    }

    void Update()
    {
        if (isCountryMainPin)
        {
            if (LanguageIndex != FileManager.instance.LanguageIndex)
            {
                LanguageIndex = FileManager.instance.LanguageIndex;
                SetNameText();
            }
        }
    }

    void SetChinese()
    {
        Btn.gameObject.GetComponentInChildren<Text>().text = ChineseName;
    }

    void SetEnglish()
    {
        Btn.gameObject.GetComponentInChildren<Text>().text = Name;
    }

    public void InitProjectData(int _projectId) {
        ExelIndexName = _projectId;
        //if (gameObject.GetComponent<MoveToPin>()) { gameObject.GetComponent<MoveToPin>().ExelIndex = ExelIndexName; }
        if (isCountryMainPin)
            SetNameText();
    }

    public void SetNameText()
    {
        projectParam = FileManager.instance.GetExceleRow(LanguageIndex, ExelIndexName);
        string _tmpName = projectParam.Project;
        _tmpName = _tmpName.Replace("<En>", "");
        _tmpName = _tmpName.Replace("</En>", " ");
        Btn.gameObject.GetComponentInChildren<Text>().text = _tmpName;
        Vector2 newSize = new Vector2(Btn.gameObject.GetComponentInChildren<Text>().preferredWidth, Btn.transform.parent.GetComponent<RectTransform>().sizeDelta.y);
        newSize.x = newSize.x + 25;
        Btn.transform.parent.GetComponent<RectTransform>().sizeDelta = newSize;
    }

    private void FixedUpdate()
    {
        if (lookCamera != null) {
            quad.transform.LookAt(quad.transform.position + lookCamera.transform.rotation * Vector3.forward, lookCamera.transform.rotation * Vector3.up);
        }
    }


    void SetFilterByCluster()
    {
        if (ThirdCamera.instance.choosArea)
        {
            LeftPanelScript.instance.ShowProjectsByArray(ProjectsIndexes);
        }
    }

    public void ShowPin(float _delay)
    {
        iTween.ScaleTo(ItemContainer, iTween.Hash("scale", Vector3.one, "delay", _delay, "time", 0.5f));
    }

    public void HidePin(float _delay)
    {
        iTween.ScaleTo(ItemContainer, iTween.Hash("scale", Vector3.zero, "delay", _delay, "time", 0.5f));
    }

    public void Select()
    {
        Btn.gameObject.GetComponent<Image>().sprite = selectImg;
        Btn.gameObject.GetComponentInChildren<Text>().color = Color.white;
    }

    public void UnSelect()
    {
        Btn.gameObject.GetComponent<Image>().sprite = normalImg;
        Btn.gameObject.GetComponentInChildren<Text>().color = Color.black;
    }

    public void SetScale(float _i)
    {
        ItemContainer.transform.localScale = Vector3.Lerp(Vector3.one, new Vector3(0.6f, 0.6f, 0.6f), _i);
    }
}
