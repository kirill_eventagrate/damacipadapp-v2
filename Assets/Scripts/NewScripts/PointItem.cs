﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointItem : MonoBehaviour
{

    public GameObject FocusCamera;
    public GameObject Point;
    public GameObject PointButton;
    public Button btn;
    public int AreaIndex;
    public float sizeRatio = 0;
    public int AreaExcelIndex;

    
    int LanguageIndex = 0;

    Animator animator;
    Vector2 hideSize = new Vector2(32.26f, 30f);
    Vector2 openSize;
    Entity_English.AreaParam areaParam = null;

    // Start is called before the first frame update
    void Start()
    {
        openSize = PointButton.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta;
        btn.onClick.AddListener(() => ClickOnPoint());
        if (gameObject.GetComponent<Animator>())
        {
            animator = gameObject.GetComponent<Animator>();
        }
        //SetNameText();
    }

    void Update()
    {
        if (LanguageIndex != FileManager.instance.LanguageIndex)
        {
            LanguageIndex = FileManager.instance.LanguageIndex;
            SetNameText();
        }
    }

    public void SetAreaData(int _areaId)
    {
        AreaExcelIndex = _areaId;
        //areaParam = FileManager.instance.GetAreaParam(LanguageIndex, AreaExcelIndex);
        ThirdCamera.instance.Areas[AreaIndex].GetComponent<AreaScript>().SetAreaData(AreaExcelIndex);

        SetNameText();
    }

    public void SetNameText()
    {
        areaParam = FileManager.instance.GetAreaParam(LanguageIndex, AreaExcelIndex);
        string _tmpName = areaParam.Area;
        _tmpName = _tmpName.Replace("<En>", "");
        _tmpName = _tmpName.Replace("</En>", " ");
        btn.gameObject.GetComponentInChildren<Text>().text = _tmpName;
        Vector2 newSize = new Vector2(btn.gameObject.GetComponentInChildren<Text>().preferredWidth, 30);
        newSize.x = newSize.x + 50;
        openSize = newSize;
        PointButton.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = newSize;
    }

    private void LateUpdate()
    {
        PointButton.transform.LookAt(PointButton.transform.position + FocusCamera.transform.rotation * Vector3.forward, FocusCamera.transform.rotation * Vector3.up);
        PointButton.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = Vector2.Lerp(openSize, hideSize, sizeRatio);
    }

    public void ShowPoint(float _delay)
    {
        iTween.ScaleTo(Point, iTween.Hash("scale", Vector3.one, "delay", _delay, "time", 0.5f, "easetype", iTween.EaseType.easeOutBack)); // easeOutBack
    }

    public void HidePoint(float _delay)
    {
        iTween.ScaleTo(Point, iTween.Hash("scale", Vector3.zero, "delay", _delay, "time", .8f, "easetype", iTween.EaseType.easeInBack)); // easeInBack
    }

    void ClickOnPoint()
    {
        if (!ThirdCamera.instance.choosArea)
        {
            Vector3 tmpPos = new Vector3(transform.position.x, transform.position.y + 0.1f, transform.position.z - 0.05f);
            SecondCamera.instance.SaveLastPos();
            SecondCamera.instance.MoveTo(tmpPos, 1.2f, 0);
            ThirdCamera.instance.SetActiveAreaByIndex(AreaIndex);
            ThirdCamera.instance.StartMove(1f, 0.2f, 0f);

            
            UIManager.instance.PutLegendBtn(AreaExcelIndex, ExcelParam.Area, () => {});
        }
    }

    public void AnimationShow()
    {
        if (animator)
        {
            animator.SetFloat("Direction", -1f);
            animator.Play("ShowHidePin", -1, 1);
        }
    }

    public void AnimationHide()
    {
        if (animator)
        {
            animator.SetFloat("Direction", 1f);
            animator.Play("ShowHidePin", -1, 0);
        }
    }

    public void PinScale(float _i)
    {
        //Point.transform.localScale = Vector3.Lerp(Vector3.one, new Vector3(0.6f, 0.6f, 0.6f), _i);
    }
}
