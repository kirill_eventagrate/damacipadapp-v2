﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountryScript : MonoBehaviour
{

    public CityScript[] Citys;
    public GameObject StartTarget;
    public GameObject EndTarget;

    int CountryExcelId;
    public Entity_English.CountryParam countryParam = null;
    private void Awake()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetCountryData(int _countryId) {
        CountryExcelId = _countryId;
        countryParam = FileManager.instance.GetCountryParam(FileManager.instance.LanguageIndex, CountryExcelId);

        string[] cityIdFilters = countryParam.CityIdFilters.Split(',');
        for (int i = 0; i < Citys.Length; i++)
        {
            if (i < cityIdFilters.Length) {
                int cityId = System.Convert.ToInt32(cityIdFilters[i].Trim());
                Citys[i].InitCityData(cityId);
            }
            
            //SecondCamera.instance.Countries[CountryIndex].Citys[i].InitAreasData(cityId);
        }
    }

    public void UpdateLanguage() {
        countryParam = FileManager.instance.GetCountryParam(FileManager.instance.LanguageIndex, CountryExcelId);
    }
    
}


