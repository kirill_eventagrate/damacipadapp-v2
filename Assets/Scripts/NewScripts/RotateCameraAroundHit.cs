﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Lean.Touch {
    public class RotateCameraAroundHit : MonoBehaviour
    {
        [Tooltip("Ignore fingers with StartedOverGui?")]
        public bool IgnoreStartedOverGui = true;

        [Tooltip("Ignore fingers with IsOverGui?")]
        public bool IgnoreIsOverGui;

        [Tooltip("Allows you to force rotation with a specific amount of fingers (0 = any)")]
        public int RequiredFingerCount;

        [Tooltip("Does rotation require an object to be selected?")]
        public LeanSelectable RequiredSelectable;

        [Tooltip("The camera we will be used to calculate relative rotations (None = MainCamera)")]
        public Camera Camera;

        [Tooltip("Should the rotation be performanced relative to the finger center?")]
        public bool Relative;

        public float Smooth;
        public bool canUse = false;
        //public GameObject Sphere;

        int lastFingersCount = 0;
        Vector3 HitPoint = Vector3.zero;
        //float Angle = 0;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        private void LateUpdate()
        {
            if (canUse)
            {
                var fingers = LeanSelectable.GetFingersOrClear(IgnoreStartedOverGui, IgnoreIsOverGui, RequiredFingerCount, RequiredSelectable);

                // Calculate the rotation values based on these fingers
                var twistDegrees = LeanGesture.GetTwistDegrees(fingers);

                if ((lastFingersCount != fingers.Count) && (fingers.Count == 2)) {
                    
                }
                lastFingersCount = fingers.Count;

                if (fingers.Count == 2)
                {
                    if (Mathf.Abs(twistDegrees) >= 0.4f)
                    {
                        // Make sure the camera exists
                        var camera = LeanTouch.GetCamera(Camera, gameObject);

                        if (camera != null)
                        {
                            var axis = transform.InverseTransformDirection(camera.transform.up);

                            Vector3 axis2 = new Vector3(0, twistDegrees, 0);

                            //================================================
                            Ray ray = gameObject.GetComponent<Camera>().ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
                            RaycastHit hit;
                            Vector3 FingersCenterPos = (fingers[0].GetWorldPosition(0.2f, camera) + fingers[1].GetWorldPosition(0.2f, camera)) / 2;
                            Vector3 Direction = FingersCenterPos - transform.position;
                            if (Physics.Raycast(transform.position, Direction, out hit, 1500))
                            {
                                HitPoint = hit.point;
                            }
                            //================================================


                            Debug.DrawLine(transform.position, HitPoint);
                            if (Camera.gameObject.GetComponent<SecondCamera>()) {
                                SecondCamera.instance.GetCurrentCountry().gameObject.transform.RotateAround(HitPoint, Vector3.up, -twistDegrees);
                                //transform.LookAt(HitPoint);
                            }

                            if (Camera.gameObject.GetComponent<ThirdCamera>())
                            {
                                ThirdCamera.instance.GetCurrentArea().gameObject.transform.RotateAround(HitPoint, Vector3.up, -twistDegrees);
                                //transform.LookAt(HitPoint);
                            }

                        }
                        else
                        {
                            Debug.LogError("Failed to find camera. Either tag your cameras MainCamera, or set one in this component.", this);
                        }
                    }
                    if (twistDegrees >= 0.4f)
                    {
                        gameObject.GetComponent<LeanCameraZoom>().canUse = false;
                    }
                    else
                    {
                        gameObject.GetComponent<LeanCameraZoom>().canUse = true;
                    }
                }
                

                
            }
        }
    }
}


