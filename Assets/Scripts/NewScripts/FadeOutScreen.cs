﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeOutScreen : MonoBehaviour
{

    static public FadeOutScreen instance = null;

    public Camera First;
    public Camera Second;
    public Camera Third;
    public Image FadeInOutImage;

    public RawImage FirstCam;
    public RawImage SecondCam;
    public RawImage ThirdCam;

    //[HideInInspector]
    public int levelIndex = 1;

    void Awake()
    {
        if (instance == null)
            instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    

    public void StartFade(string _type, float _delay)
    {
        //fade = !fade;
        StartCoroutine(FadeInOutAnimation(_type, _delay));
        //SecondCamera.instance.StartMove();
    }

    public void ChandeCameraFromSecondToFirst(float _delay)
    {
        StartCoroutine(FromSecondToFirst(_delay));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator FadeInOutAnimation(string _type, float _delay)
    {
        yield return new WaitForSeconds(_delay);
        for (float i = 0; i < 1.1; i += 0.05f)
        {
            float transparent = Mathf.Lerp(0, 1, i);
            Color color1 = new Color(FadeInOutImage.color.r, FadeInOutImage.color.g, FadeInOutImage.color.b, transparent);
            FadeInOutImage.color = color1;
            //yield return new WaitForSeconds(0.01f);
            yield return new WaitForFixedUpdate();
        }
        yield return new WaitForSeconds(0.1f);
        switch (_type)
        {
            case "fromFirstToSecond":
                First.enabled = false;
                Second.enabled = true;
                Third.enabled = false;
                levelIndex = 2;
                break;
            case "fromSecondToThird":
                First.enabled = false;
                Second.enabled = false;
                Third.enabled = true;
                levelIndex = 3;
                break;
            case "fromThirdToSecond":
                First.enabled = false;
                Second.enabled = true;
                Third.enabled = false;
                levelIndex = 2;
                break;
            case "fromSecondToFirst":
                First.enabled = true;
                Second.enabled = false;
                Third.enabled = false;
                levelIndex = 1;
                break;
        }
        
        for (float i = 0; i < 1.1; i += 0.05f)
        {
            float transparent = Mathf.Lerp(1, 0, i);
            Color color1 = new Color(FadeInOutImage.color.r, FadeInOutImage.color.g, FadeInOutImage.color.b, transparent);
            FadeInOutImage.color = color1;
            yield return new WaitForSeconds(0.01f);
        }
    }


    IEnumerator FromFirstToSecond(float _delay)
    {
        yield return new WaitForSeconds(_delay);
        /*for (float i = 0; i < 1.1; i += 0.05f)
        {
            float firstTransparent = Mathf.Lerp(1, 0, i);
            float first_r = FirstCam.color.r;
            float first_g = FirstCam.color.g;
            float first_b = FirstCam.color.b;
            Color color1 = new Color(Mathf.Lerp(first_r, 0, i), Mathf.Lerp(first_g, 0, i), Mathf.Lerp(first_b, 0, i), firstTransparent);
            FirstCam.color = color1;
            yield return new WaitForSeconds(0.01f);
        }
        for (float i = 0; i < 1.1; i += 0.05f)
        {
            float secondTransparent = Mathf.Lerp(0, 1, i);
            float second_r = 1;
            float second_g = 1;
            float second_b = 1;
            Color color2 = new Color(Mathf.Lerp(0, second_r, i), Mathf.Lerp(0, second_g, i), Mathf.Lerp(0, second_b, i), secondTransparent);
            SecondCam.color = color2;
            yield return new WaitForSeconds(0.01f);
        }*/

        
    }

    IEnumerator FromSecondToFirst(float _delay)
    {
        yield return new WaitForSeconds(_delay);
        for (float i = 0; i < 1.1; i += 0.025f)
        {
            float firstTransparent = Mathf.Lerp(0, 1, i);
            float secondTransparent = Mathf.Lerp(1, 0, i);
            Color color1 = new Color(FirstCam.color.r, FirstCam.color.g, FirstCam.color.b, firstTransparent);
            FirstCam.color = color1;
            Color color2 = new Color(SecondCam.color.r, SecondCam.color.g, SecondCam.color.b, secondTransparent);
            SecondCam.color = color2;
            yield return new WaitForSeconds(0.01f);
        }
    }

    public void BeginFadeFromSecondToThird(float _delay)
    {
        StartCoroutine(FromSecondToThird(_delay));
    }

    IEnumerator FromSecondToThird(float _delay)
    {
        yield return new WaitForSeconds(_delay);
        for (float i = 0; i < 1.1; i += 0.05f)
        {
            float secondTransparent = Mathf.Lerp(1, 0, i);
            float first_r = 0.7607844f;
            float first_g = 0.9764706f;
            float first_b = 1f;
            Color color1 = new Color(Mathf.Lerp(1, first_r, i), Mathf.Lerp(1, first_g, i), Mathf.Lerp(1, first_b, i), 1);
            SecondCam.color = color1;
            yield return new WaitForSeconds(0.01f);
        }
        /*for (float i = 0; i < 1.1; i += 0.05f)
        {
            float thirdTransparent = Mathf.Lerp(0, 1, i);
            float second_r = 0.7607844f;
            float second_g = 0.9764706f;
            float second_b = 1f;
            Color color2 = new Color(Mathf.Lerp(second_r, 1, i), Mathf.Lerp(second_g, 1, i), Mathf.Lerp(second_b, 1, i), thirdTransparent);
            ThirdCam.color = color2;
            yield return new WaitForSeconds(0.01f);
        }*/
    }

    IEnumerator FromThirdToSecond(float _delay)
    {
        yield return new WaitForSeconds(_delay);
        for (float i = 0; i < 1.1; i += 0.025f)
        {
            float secondTransparent = Mathf.Lerp(0, 1, i);
            float thirdTransparent = Mathf.Lerp(1, 0, i);
            Color color1 = new Color(SecondCam.color.r, SecondCam.color.g, SecondCam.color.b, secondTransparent);
            SecondCam.color = color1;
            Color color2 = new Color(ThirdCam.color.r, ThirdCam.color.g, ThirdCam.color.b, thirdTransparent);
            ThirdCam.color = color2;
            yield return new WaitForSeconds(0.01f);
        }
    }

    public void BeginFadeFromThirdToSecond(float _delay)
    {
        StartCoroutine(FromThirdToSecond(_delay));
    }
}
