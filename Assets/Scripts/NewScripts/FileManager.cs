﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json.Linq;





public class FileManager : MonoBehaviour {

    [HideInInspector]
    static public FileManager instance = null;

    public Entity_English exelData;
    public int LanguageIndex = 0;

    [SerializeField]
    public JObject Settings;

    string Path = "PinsContent/";


    private void Awake()
    {
        if (instance == null)
            instance = this;

        GetSettings();
        ExcelImportRunTime excelImportRunTime = new ExcelImportRunTime();
        exelData = excelImportRunTime.ReadExcel(exelData, Settings["ExcelFile"].ToString());
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.I))
        {
            
        }
	}

    public void SetLanguageIndex(int _LanguageIndex)
    {
        Debug.Log("_LanguageIndex: " + _LanguageIndex);
        //LanguageIndex = _LanguageIndex;
    }

    void GetSettings()
    {
        string root = Application.persistentDataPath + "/";
        string filePath = root + "Settings.txt";
        //Debug.Log(filePath);
        System.IO.StreamReader reader = new StreamReader(filePath, System.Text.Encoding.GetEncoding(1252));
        string text = reader.ReadToEnd();
        reader.Close();
        Settings = JObject.Parse(text);
    }

    public void GetRootFolder()
    {
        Debug.Log(UnityEngine.Application.persistentDataPath + "/");
        
    }

    string GetPathFromExel(int _index)
    {
        string result = "";
        if (_index < exelData.sheets[0].Projectslist.Count)
        {
            result = exelData.sheets[0].Projectslist[_index].Country + "/";
            if (exelData.sheets[0].Projectslist[_index].Area.Trim() != "")
            {
                result += exelData.sheets[0].Projectslist[_index].City + "/" + exelData.sheets[0].Projectslist[_index].Area + "/";
            }
            result += exelData.sheets[0].Projectslist[_index].Project + "/";
        }
        return result;
    }

    public Dictionary<string, List<string>> GetDataForPin(string _pinPath)
    {
        //rowIndex = rowIndex - 2;
        Dictionary<string, List<string>> result = new Dictionary<string, List<string>>();
        string root = UnityEngine.Application.persistentDataPath + "/" + Path;
        string path = root + _pinPath; // GetPathFromExel(rowIndex);
        //Debug.Log(path);
        
        
        if (Directory.Exists(path))
        {
            result.Add("info", GetFileList(path + "00_Info/", new[] { "*.txt" }));
            result.Add("video", GetFileList(path + "01_Video/", new[] { "*.mp4"  }));
            result.Add("interior", GetFileList(path + "02_Images Interior/", new[] { "*.jpeg", "*.jpg", "*.png" }));
            result.Add("exterior", GetFileList(path + "03_Images Exterior/", new[] { "*.jpeg", "*.jpg", "*.png" }));
            result.Add("plan", GetFileList(path + "04_Floorplan/", new[] { "*.jpeg", "*.jpg", "*.png" }));

            //result.Add("brochure", GetFileList(path + "06_Brochure/", new[] { "*.txt" }));
            result.Add("brochure", new List<string>());

            result.Add("logo", GetFileList(path + "10_Logo/", new[] { "*.jpeg", "*.jpg", "*.png" }));
            result.Add("masterPic", GetFileList(path + "11_MasterPic/", new[] { "*.jpeg", "*.jpg", "*.png" }));
        }
        return result;
    }

    public List<string> GetFileList(string path, string[] fileTypes){
        List<string> resultArray = new List<string>();
        if (Directory.Exists(path))
        {
            var info = new DirectoryInfo(path);
            foreach (string fileType in fileTypes)
            {
                FileInfo[] fileList;
                fileList = info.GetFiles(fileType);
                foreach (var file in fileList)
                {
                    resultArray.Add(file.ToString());
                }
            }
        }
        return resultArray;
    }

    public Entity_English.ProjectParam GetChiniseText(int _index)
    {
        _index = _index - 2;
        if (_index < 0)
        {
            _index = 0;
        }
        Entity_English.ProjectParam result = null;
        if (_index < exelData.sheets[1].Projectslist.Count)
        {
            result = exelData.sheets[1].Projectslist[_index];
        }
        return result;
    }

    public Entity_English.ProjectParam GetExceleRow(int _sheetIndex, int _index)
    {
        _index = _index - 1;
        if (_index < 0)
        {
            _index = 0;
        }
        Entity_English.ProjectParam result = null;
        if (_index < exelData.sheets[_sheetIndex].Projectslist.Count)
        {
            result = exelData.sheets[_sheetIndex].Projectslist[_index];
        }
        return result;
    }

    public Entity_English.CountryParam GetCountryParam(int _sheetIndex, int _index) {
        return exelData.sheets[_sheetIndex].Coutrieslist[_index - 1];
    }

    public Entity_English.CityParam GetCityParam(int _sheetIndex, int _index)
    {
        return exelData.sheets[_sheetIndex].Cityslist[_index - 1];
    }

    public Entity_English.AreaParam GetAreaParam(int _sheetIndex, int _index)
    {
        return exelData.sheets[_sheetIndex].Areaslist[_index - 1];
    }
    public Entity_English.BrochureListParam GetBroshureByProjectId(int _index) {
        return exelData.Brochurelist[_index - 1];
    }
}
