﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaScript : MonoBehaviour
{
    static public AreaScript instance = null;

    public AreaType areaType;
    public GameObject EndTarget;
    public Vector2 XLimit = new Vector2(-0.2f, 0.1f);
    public Vector2 YLimit = new Vector2(-0.1f, 0);
    public Vector2 ZLimit = new Vector2(-0.1f, 0.1f);
    public Vector2 ZoomLimit = new Vector2(0, 0.2f);
    public GameObject[] Points;

    public Vector3 startPos;
    public Vector3 startRotation;
    List<ContentItem> ContentList = new List<ContentItem>();
    Entity_English.AreaParam areaParam = new Entity_English.AreaParam();
    int AreaId = 0;
    public List<int> ProjectsList = new List<int>();

    void Awake()
    {
        if (instance == null)
            instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
        startRotation = transform.eulerAngles;
    }

    public void SetAreaData(int _areaId) {
        AreaId = _areaId;
        areaParam = FileManager.instance.GetAreaParam(FileManager.instance.LanguageIndex, AreaId);
        string[] projectsIdFilters = areaParam.ProjectIdFilters.Split(',');
        ProjectsList.Clear();
        for (int i = 0; i < projectsIdFilters.Length; i++)
        {
            int projectId = System.Convert.ToInt32(projectsIdFilters[i].Trim());
            ProjectsList.Add(projectId);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InitArea()
    {
        if (ContentList.Count == 0)
        {
            if (areaType == AreaType.Simple)
            {

                foreach (GameObject point in Points)
                {
                    if (point.GetComponent<ProjectItem>().content.ExelIndexName != 0)
                    {
                        ContentList.Add(new ContentItem(point.GetComponent<ProjectItem>().content.ExelIndexName));
                    }
                }
            }
            else if (areaType == AreaType.DamacHills)
            {
                ContentList.Add(new ContentItem(40)); // 0
                ContentList.Add(new ContentItem(41));// 1
                ContentList.Add(new ContentItem(42));// 2
                ContentList.Add(new ContentItem(43)); // 3
                ContentList.Add(new ContentItem(44)); // 4
                ContentList.Add(new ContentItem(45)); // 5
                ContentList.Add(new ContentItem(46)); // 6
                ContentList.Add(new ContentItem(47)); // 7
                ContentList.Add(new ContentItem(48)); // 8
                ContentList.Add(new ContentItem(49)); // 9
                ContentList.Add(new ContentItem(50)); // 10
                ContentList.Add(new ContentItem(51)); // 11
                ContentList.Add(new ContentItem(52)); // 12
                ContentList.Add(new ContentItem(53)); // 13
                ContentList.Add(new ContentItem(54)); // 10
                ContentList.Add(new ContentItem(55)); // 11
                ContentList.Add(new ContentItem(56)); // 12
                ContentList.Add(new ContentItem(57)); // 13
                ContentList.Add(new ContentItem(58)); // 10
                ContentList.Add(new ContentItem(59)); // 11
                ContentList.Add(new ContentItem(60)); // 12
                ContentList.Add(new ContentItem(61)); // 13
                ContentList.Add(new ContentItem(62)); // 10
                ContentList.Add(new ContentItem(63)); // 11
                ContentList.Add(new ContentItem(64)); // 12
                ContentList.Add(new ContentItem(65)); // 13
            }
            else if (areaType == AreaType.Akoya)
            {
                ContentList.Add(new ContentItem(3)); // 0
                ContentList.Add(new ContentItem(4)); // 1
                ContentList.Add(new ContentItem(5)); // 2
                ContentList.Add(new ContentItem(6)); // 3
                ContentList.Add(new ContentItem(7)); // 4
                ContentList.Add(new ContentItem(8)); // 5
                ContentList.Add(new ContentItem(9)); // 6
                ContentList.Add(new ContentItem(10)); // 7
                ContentList.Add(new ContentItem(11)); // 8
                ContentList.Add(new ContentItem(12)); // 9
                ContentList.Add(new ContentItem(13)); // 10
                ContentList.Add(new ContentItem(14)); // 11
                ContentList.Add(new ContentItem(15)); // 11
            }
        }
        
        //Debug.Log("ContentList: " + ContentList.Count);
        
    }

    public List<ContentItem> GetContents()
    {
        UnSelectAllPins();
        return ContentList;
    }

    public void UnSelectAllPins()
    {
        foreach (GameObject point in Points)
        {
            point.GetComponent<ProjectItem>().UnSelect();
        }
    }
    public void SetPointsScale(float _i)
    {
        foreach (GameObject point in Points)
        {
            point.GetComponent<ProjectItem>().SetScale(_i);
        }
    }

    public void SetStartRotation() {
        transform.position = startPos;
        transform.eulerAngles = startRotation;
    }

    public List<int> GetProjectsList() { return ProjectsList; }
}

public enum AreaType
{
    Simple,
    DamacHills,
    Akoya
}
