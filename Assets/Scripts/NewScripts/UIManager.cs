﻿using Lean.Touch;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    static public UIManager instance = null;

    public string WorldtitleEnglish;
    public string WorldtitleChinese;
    public GameObject LegendContainer;
    public GameObject LegendBtn;
    public GameObject ImageSliderContainer;
    public GameObject ImageSlider;
    public Button ShowHideFavoritesBtn;


    List<Vector2> positions = new List<Vector2>();
    List<GameObject> Sliders = new List<GameObject>();

    void Awake()
    {
        if (instance == null)
            instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        positions.Add(new Vector2(-320, 675));
        positions.Add(new Vector2(-320, 245));
        positions.Add(new Vector2(-320, -185));
        LegendContainer.transform.GetChild(0).gameObject.GetComponent<LegendItem>().Names.Add(WorldtitleEnglish);
        LegendContainer.transform.GetChild(0).gameObject.GetComponent<LegendItem>().Names.Add(WorldtitleChinese);//FileManager.instance.GetChiniseText(2).Heirarchy;
        LegendContainer.transform.GetChild(0).gameObject.GetComponent<LegendItem>().SetAction(() => {
            ThirdCamera.instance.ResetState();
            if (FadeOutScreen.instance.levelIndex == 3)
            {
                SecondCamera.instance.MoveToStartPos(0.5f);
            }
            else if (FadeOutScreen.instance.levelIndex == 2)
            {
                SecondCamera.instance.MoveToStartPos(0);
            }
            SecondCamera.instance.ResetState(false);
            FadeOutScreen.instance.StartFade("fromSecondToFirst", 0);
            FirstCamera.instance.MoveBack(0);
        });
        LegendContainer.transform.GetChild(0).gameObject.GetComponent<LegendItem>().SetEnableState(false);
        ShowHideFavoritesBtn.onClick.AddListener(() => FavoritelistScript.instance.ShowHideFavoritesList());
        LeftPanelScript.instance.ResetState();
        RightPanelScript.instance.ResetState();

        RemoveLastItem(0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RemoveLastItem(int _index)
    {
        for (int i = 0; i < LegendContainer.transform.childCount; i++)
        {
            if (i > _index)
            {
                Destroy(LegendContainer.transform.GetChild(i).gameObject);
            }
        }
        Loom.QueueOnMainThread(() => {
            ResetLegendItemsState();
        }, 0.05f);
    }

    public int GetLegendItemsCount() { return LegendContainer.transform.childCount; }

    public void PutLegendBtn(int ExcelRowIndex, ExcelParam excelParam, Action _action)
    {
        GameObject btn = Instantiate(LegendBtn) as GameObject;
        btn.transform.SetParent(LegendContainer.transform);
        Vector2 newSize = new Vector2(btn.transform.GetChild(1).gameObject.GetComponent<Text>().preferredWidth, 100);
        btn.GetComponent<RectTransform>().sizeDelta = newSize;
        btn.GetComponent<LegendItem>().SetAction(_action);
        btn.GetComponent<LegendItem>().ExcelRowIndex = ExcelRowIndex;
        btn.GetComponent<LegendItem>().excelParam = excelParam;
        /*btn.GetComponent<LegendItem>().EnglishName = _text;
        btn.GetComponent<LegendItem>().ChineseName = _chineseName;*/
        btn.GetComponent<LegendItem>().SetNameText();
        ResetLegendItemsState();
    }

    public void AddImageSlider(List<string> _files, SliderType _sliderType)
    {
        //FileManager.instance.GetRootFolder();
        GameObject obj = Instantiate(ImageSlider) as GameObject;
        obj.transform.SetParent(ImageSliderContainer.transform);
        Vector2 _position = positions[0];
        int matchesCount = 0;
        foreach (Vector2 pos in positions)
        {
            matchesCount = 0;
            foreach (GameObject slider in Sliders)
            {
                if (slider.GetComponent<RectTransform>().anchoredPosition == pos)
                {
                    matchesCount++;
                }
            }
            if (matchesCount == 0)
            {
                _position = pos;
                break;
            }
        }
        obj.GetComponent<RectTransform>().anchoredPosition = _position;
        obj.GetComponent<ImageSlider>().SetImgArray(_files, Sliders.Count);
        obj.GetComponent<ImageSlider>().sliderType = _sliderType;
        //obj.
        Sliders.Add(obj);
    }

    public void SetSlideOrder(GameObject _slider)
    {
        foreach (GameObject slider in Sliders)
        {
            if (slider == _slider)
            {
                slider.GetComponent<Canvas>().sortingOrder = 11;
            }
            else
            {
                slider.GetComponent<Canvas>().sortingOrder = 10;
            }
        }
    }

    public void SetImageFromFile(Image img, string file)
    {
        img.sprite = null;
        Texture2D tex = null;
        byte[] fileData;
        string filePath = file;
        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData);
            Sprite mySprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
            img.sprite = mySprite;
            img.preserveAspect = true;
        }
    }

    public void CorrectImageSize(Image img, string file)
    {
        img.sprite = null;
        Texture2D tex = null;
        byte[] fileData;
        string filePath = file;
        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData);
            Sprite mySprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
            img.sprite = mySprite;
            img.preserveAspect = true;
            AspectRatioFitter aspectRatioFitter = img.gameObject.GetComponent<AspectRatioFitter>();
            float aspectRatio = 1.44241f;
            aspectRatio = (float)tex.width / (float)tex.height;
            aspectRatioFitter.aspectRatio = aspectRatio;
            //Debug.Log("aspectRatio: " + aspectRatio);
            if (aspectRatio < 1)
            {
                RightPanelScript.instance.ShowHideMainImageBtn.gameObject.SetActive(false);
            }
            else
            {
                RightPanelScript.instance.ShowHideMainImageBtn.gameObject.SetActive(true);
            }
        }
    }

    public void ResetLegendItemsState()
    {
        if (LegendContainer.transform.childCount > 0)
        {
            for (int i = 0; i < LegendContainer.transform.childCount - 1; i++)
            {
                LegendContainer.transform.GetChild(i).GetComponent<LegendItem>().SetEnableState(true);
            }
            int j = LegendContainer.transform.childCount - 1;
            LegendContainer.transform.GetChild(j).GetComponent<LegendItem>().SetEnableState(false);
            //Debug.Log("index: " + j);
        }
    }

    public void RemoveSlider(SliderType _sliderType)
    {
        foreach (GameObject slider in Sliders)
        {
            if (slider.GetComponent<ImageSlider>().sliderType == _sliderType)
            {
                slider.GetComponent<ImageSlider>().HideSlider();
                Sliders.Remove(slider);
                break;
            }
        }
    }
}
