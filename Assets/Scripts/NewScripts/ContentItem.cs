﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ContentItem
{
    public string Path;
    public int ExelIndexName;

    public List<string> Names;

    public ContentItem() { }

    public ContentItem(int _exelIndexName) {
        SetData(_exelIndexName);
    }

    

    public void SetData(int _exelIndexName)
    {
        ExelIndexName = _exelIndexName;
        Path = FileManager.instance.Settings["PinsFolder"][_exelIndexName - 1]["Path"].ToString();
        
    }

    public void ClickOnBtn()
    {
        RightPanelScript.instance.ShowHideMenu(Path, ExelIndexName);
        
        //Debug.Log(FileManager.instance.GetChiniseText(Path));
    }
}