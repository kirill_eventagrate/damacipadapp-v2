﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LanguageTextchanger : MonoBehaviour
{
    public Text textField;
    public string[] LanguageText;

    int LanguageIndex = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (LanguageIndex != FileManager.instance.LanguageIndex)
        {
            LanguageIndex = FileManager.instance.LanguageIndex;
            SetText();
        }
    }

    void SetText() {
        if (LanguageIndex < LanguageText.Length) {
            textField.text = LanguageText[LanguageIndex];
        }
    }

}
