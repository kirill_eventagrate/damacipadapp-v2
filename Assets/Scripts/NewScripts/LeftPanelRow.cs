﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeftPanelRow : MonoBehaviour
{
    public Text Number;
    public Text Title;

    [HideInInspector]
    public string Name;
    public ContentItem content = new ContentItem();
    Button btn;
    public Button favoriteBtn;
    Animator animator;
    public Animator FavouriteMessageAnim;
    public Text FavourteMessageText;

    [HideInInspector]
    public bool isShow = true;

    
    public Sprite NormalHeartIcon;
    public Sprite SelectHeartIcon;

    int LanguageIndex = 0;

    public Image buttonBgImage;
    Color defaultBgColor;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        btn = gameObject.GetComponent<Button>();
        btn.onClick.AddListener(() => content.ClickOnBtn());
        btn.onClick.AddListener(() => SetBgImageState());
        favoriteBtn.onClick.AddListener(() => SetFavorite());
        defaultBgColor = buttonBgImage.color;
        SetText();
    }

    void SetBgImageState()
    {
        foreach(Transform item in transform.parent)
        {
            item.GetComponent<LeftPanelRow>().SetDefaultBgImageColor();
        }
        buttonBgImage.color = Color.white;
    }

    public void SetDefaultBgImageColor()
    {
        buttonBgImage.color = defaultBgColor;
    }
    void SetFavorite()
    {
        if (!FavoritelistScript.instance.HasFavorites(content.ExelIndexName))
        {
            FavoritelistScript.instance.AddFavorite(content);
            favoriteBtn.gameObject.GetComponent<Image>().sprite = SelectHeartIcon;
            FavourteMessageText.text = "Added to favourites";

        }
        else
        {
            FavoritelistScript.instance.RemoveFavorite(content);
            UnSetFavorite();
            FavourteMessageText.text = "Removed from favourites";
        }
        FavouriteMessageAnim.Play("FavouritesMessage", -1, 0);
    }

    public void UnSetFavorite()
    {
        favoriteBtn.gameObject.GetComponent<Image>().sprite = NormalHeartIcon;
    }

    // Update is called once per frame
    void Update()
    {
        if (LanguageIndex != FileManager.instance.LanguageIndex)
        {
            LanguageIndex = FileManager.instance.LanguageIndex;
            SetText();
        }
    }

    public void SetData(string _number, int _ExelIndexName)
    {
        Number.text = _number;
        //Debug.Log("_content.ExelIndexName: " + _content.ExelIndexName);
        content.SetData(_ExelIndexName);

        SetText();
        if (FavoritelistScript.instance.HasFavorites(_ExelIndexName))
        {
            favoriteBtn.gameObject.GetComponent<Image>().sprite = SelectHeartIcon;
            //Debug.Log("has in favorites");
        }
    }

    

    void SetText()
    {
        string _tmpName = FileManager.instance.GetExceleRow(LanguageIndex, content.ExelIndexName).Project;
        _tmpName = _tmpName.Replace("<En>", "");
        _tmpName = _tmpName.Replace("</En>", " ");
        Title.text = _tmpName; // _tmpName
    }

    public void ShowRow()
    {
        isShow = true;
        animator.SetFloat("Direction", -1f);
        animator.Play("ShowHideContent", -1, 1f);
    }
    public void HideRow()
    {
        isShow = false;
        animator.SetFloat("Direction", 1f);
        animator.Play("ShowHideContent", -1, 0f);
    }
}
