﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ImageSlider : MonoBehaviour
{
    public GameObject leftBtn;
    public GameObject rightBtn;
    public GameObject closeBtn;
    public Image BigImg;
    public int sliderIndex = 0;
    public SliderType sliderType;
    public Text SlideCounter;
    List<string> imgs = new List<string>();
    int currectIndex = 0;
    Vector2 ClosePos = new Vector2(3000, 0);
    Animator leftArrowAnim;
    Animator rightArrowAnim;

    public void SetImgArray(List<string> _imgs, int _sliderIndex)
    {
        sliderIndex = _sliderIndex;
        imgs = _imgs;
        SetImages(currectIndex);
        if (imgs.Count == 1)
        {
            leftBtn.SetActive(false);
            rightBtn.SetActive(false);
        }
        else
        {
            leftBtn.SetActive(false);
            rightBtn.SetActive(true);
        }
        gameObject.GetComponent<RectTransform>().localScale = Vector3.zero;
        SlideCounter.text = (currectIndex + 1) + "/" + imgs.Count;
        iTween.ScaleTo(gameObject, iTween.Hash("scale", new Vector3(0.2768931f, 0.2768931f, 0.2768931f), "time", .3f, "easetype", iTween.EaseType.easeOutBack));
    }
         

    public void SetImages(int correctImgIndex)
    {
        UIManager.instance.SetImageFromFile(BigImg, imgs[correctImgIndex]);
    }

    // Use this for initialization
    void Start()
    {
        leftArrowAnim = leftBtn.GetComponent<Animator>();
        rightArrowAnim = rightBtn.GetComponent<Animator>();
        leftBtn.GetComponent<Button>().onClick.AddListener(() => {
            ChangeIMage("prev");
        });
        rightBtn.GetComponent<Button>().onClick.AddListener(() => {
            ChangeIMage("next");
        });
        closeBtn.GetComponent<Button>().onClick.AddListener(() => {
            UIManager.instance.RemoveSlider(sliderType);
            RightPanelScript.instance.UnSelectBtn(sliderType);
            iTween.ScaleTo(gameObject, iTween.Hash("scale", Vector3.zero, "time", .08f, "easetype", iTween.EaseType.easeInOutSine));
        });
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void ChangeIMage(string type)
    {
        if (type == "next" && (currectIndex + 1) < imgs.Count)
        {
            currectIndex++;
            if (currectIndex == imgs.Count - 1)
            {
                leftBtn.SetActive(true);
                rightBtn.SetActive(false);
            }
            else
            {
                leftBtn.SetActive(true);
            }
        }
        else if (type == "prev" && (currectIndex - 1) >= 0)
        {
            currectIndex--;
            if (currectIndex == 0)
            {
                leftBtn.SetActive(false);
                rightBtn.SetActive(true);
            }
            else
            {
                rightBtn.SetActive(true);
            }
        }

        SlideCounter.text = (currectIndex + 1) + "/" + imgs.Count;
        UIManager.instance.SetImageFromFile(BigImg, imgs[currectIndex]);
        leftArrowAnim.Play("RightArrowAnim", -1, 0);
        rightArrowAnim.Play("RightArrowAnim", -1, 0);
    }

    public void HideSlider()
    {
        StartCoroutine(HideSliderAnim());
    }

    IEnumerator HideSliderAnim()
    {
        //yield return new WaitForSeconds(_delay);
        Vector2 pos = gameObject.GetComponent<RectTransform>().anchoredPosition;
        for (float i = 0; i < 1.1; i += 0.075f)
        {
            gameObject.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(pos, ClosePos, i);
            yield return new WaitForSeconds(0.01f);
        }
        //UIManager.instance.RemoveSlider(gameObject);
        Destroy(gameObject);
    }


}

public enum SliderType {
    Interior,
    Exterior,
    Floorplan
}