﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeftPanelScript : MonoBehaviour
{
    static public LeftPanelScript instance = null;

    public GameObject ListContainer;
    public GameObject Row;
    public InputField Search;
    public Button ShowAllProjectBtn;

    Vector2 startPos;
    List<int> Contents;
    bool isProjectListShow = false;
    Animator animator;

    bool isShowingAllProjectsButton = false;

    void Awake()
    {
        if (instance == null)
            instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        startPos = gameObject.GetComponent<RectTransform>().anchoredPosition;
        Search.onValueChanged.AddListener(delegate { SetSearchResult(Search); });
        ShowAllProjectBtn.onClick.AddListener(() => ResetPfojectListState());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetProjectList(List<int> _contents, bool search)
    {
        //if(!search) Contents = _contents;
        for (int i = 0; i < ListContainer.transform.childCount; i++)
        {
            Destroy(ListContainer.transform.GetChild(i).gameObject);
        }

        for (int i = 0; i < _contents.Count; i++)
        {
            GameObject obj = Instantiate(Row) as GameObject;
            obj.transform.SetParent(ListContainer.transform);
            obj.GetComponent<LeftPanelRow>().SetData((i + 1).ToString(), _contents[i]);
        }
    }

    void SetSearchResult(InputField input)
    {
        string searchStr = input.text;
        for (int i = 0; i < ListContainer.transform.childCount; i++)
        {
            LeftPanelRow proj = ListContainer.transform.GetChild(i).gameObject.GetComponent<LeftPanelRow>();
            if (proj.Name.ToLower().IndexOf(searchStr.ToLower()) >= 0)
            {
                if (!proj.isShow)
                {
                    proj.ShowRow();
                }
            }
            else
            {
                if (proj.isShow)
                {
                    proj.HideRow();
                }
            }
        }
    }

    public void ShowProjectsByArray(int[] _array)
    {
        for (int i = 0; i < ListContainer.transform.childCount; i++)
        {
            if (!CheckArray(i, _array))
            {
                if (ListContainer.transform.GetChild(i).gameObject.GetComponent<LeftPanelRow>().isShow)
                {
                    ListContainer.transform.GetChild(i).gameObject.GetComponent<LeftPanelRow>().HideRow();
                }
            }
            else
            {
                if (!ListContainer.transform.GetChild(i).gameObject.GetComponent<LeftPanelRow>().isShow)
                {
                    ListContainer.transform.GetChild(i).gameObject.GetComponent<LeftPanelRow>().ShowRow();
                }
            }
        }
        animator.SetFloat("Direction", 1f);
        animator.Play("ShowAllProjectBtn", -1, 0f);
        isShowingAllProjectsButton = true;
    }

    void ResetPfojectListState()
    {
        for (int i = 0; i < ListContainer.transform.childCount; i++)
        {
            if (!ListContainer.transform.GetChild(i).gameObject.GetComponent<LeftPanelRow>().isShow)
            {
                ListContainer.transform.GetChild(i).gameObject.GetComponent<LeftPanelRow>().ShowRow();
            }
        }
        animator.SetFloat("Direction", -1f);
        animator.Play("ShowAllProjectBtn", -1, 1f);
        isShowingAllProjectsButton = false;
    }

    bool CheckArray(int _item, int[] _array)
    {
        foreach (int item in _array)
        {
            if (_item == item) { return true; }
        }
        return false;
    }

    public void ShowProjectList(float _delay)
    {
        StartCoroutine(ShowList(_delay));
        isProjectListShow = true;
    }

    IEnumerator ShowList(float _delay)
    {
        yield return new WaitForSeconds(_delay);
        animator.SetFloat("Direction", 1f);
        animator.Play("LeftPanelShowHide", -1, 0f);
    }

    public void HideProjectList(float _delay)
    {
        StartCoroutine(HideList(_delay));
        isProjectListShow = false;
    }

    IEnumerator HideList(float _delay)
    {
        yield return new WaitForSeconds(_delay);
        animator.SetFloat("Direction", -1f);
        animator.Play("LeftPanelShowHide", -1, 1f);

        if (isShowingAllProjectsButton)
            Invoke("HideShowAllProjectsButton",0.5f);
    }

    void HideShowAllProjectsButton()
    {
        animator.Play("ShowAllProjectBtn", -1, 1f);
    }


    public void ResetState()
    {
        if(isProjectListShow) { HideProjectList(0f); }
    }

    public void UnSelectFavoriteInList(ContentItem _content)
    {
        for (int i = 0; i < ListContainer.transform.childCount; i++) {
            LeftPanelRow _tmpRow = ListContainer.transform.GetChild(i).gameObject.GetComponent<LeftPanelRow>();
            if (_tmpRow.content.ExelIndexName == _content.ExelIndexName)
            {
                _tmpRow.UnSetFavorite();
            }
        }
    }
}
