﻿Shader "Mix Colors by Mask" {
	Properties{
		_Color1("Main Color", Color) = (1,1,1,255)
		_Color2("Main Color", Color) = (0,0,0,255)
		_MaskTex("Mask", 2D) = "white" { }
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
#pragma surface surf Lambert

		fixed4 _Color1;
	fixed4 _Color2;
	sampler2D _MaskTex;

	struct Input {
		float2 uv_MaskTex;
	};

	void surf(Input IN, inout SurfaceOutput o) {
		fixed mask = tex2D(_MaskTex, IN.uv_MaskTex);

		fixed4 mix = lerp(_Color1, _Color2, mask);

		o.Albedo = mix.rgb;
		o.Alpha = mix.a;
	}
	ENDCG
	}
		FallBack "Diffuse"
}